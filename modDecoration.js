
exports.phenotypeGroupname2colour=function(phenGroupname) {
    let col="white";
    if (typeof phenGroupname === "undefined") return(col)

    switch(phenGroupname.toLowerCase()) {
      case "stress resistance":
      case "physiological function":
           //col = "Cornsilk";
           col = "#fff8dc";
           break;
      case "healthspan marker":
      case "healthspan biomarkers":
           //col = "lightgreen";
           col = "#90ee90";
           break;
      case "lifespan":
           //col = "orange";
           col = "#ff6500";
           break;
      case "neuronal fitness":
      case "cognitive function":
           //col = "lightgray";
           col = "#d3d3d3";
           break;
      case "body fitness":
      case "physical function":
           //col = "lightblue";
           col = "#add8e6";
           break;
      case "reproductive fitness":
      case "reproductive function":
           //col = "yellow";
           col = "#ffff00";
           break;
      default:
           //col = "white";
           col = "#ffffff";
           break;
    }
    return(col);
}

exports.simplifyGroupname=function(phenGroupname) {
   return(phenGroupname.replace("activity /","").replace("quantity /","").replace("expression of ","").replace("translocation of","").replace("velocity /","").replace("motility /","").replace("concentration of","").replace("quantity of ","").replace(" (agepigment/lipofuscin)","").replace(" (age pigment / lipofuscin)","").replace("(exon-1 huntingtin)-",""))
}
