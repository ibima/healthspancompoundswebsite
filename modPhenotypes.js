const modDecoration = require('./modDecoration');

class PhenotypeGroup {

  constructor(n) {
    this.name=n; // string
    this.phenotypes={}; // strings->objects
    this._compounds=undefined; // do not modify/read manually but use "listCompounds()", instead!
    console.log("I: Creating new phenotype group ("+n+")");
  }

  addPhenotype(phenObj) {
    this.phenotypes[phenObj.name]=phenObj;
    console.log("I: Creating adding phen "+phenObj.name+" phenotype group ("+phenObj.groupName+")");
  }

  listCompounds() {
    if ("undefined" == typeof(this._compounds)) {
      r = new Array();
      for(let p=0,l=this.phenotypes.length;i<l;i++) {
        r = r.concat(this.phenotypes[i].compounds);
      }
      this._compounds=r;
    }
    return(this._compounds);
  }

  /**
   * return "hash"/object with assignment of string to phenotypeObject - directly as stored within PhenotypeGroup
   */
  listPhenotypes() {
     return(this.phenotypes);
  }

  /**
   * Returns a list of strings with the names of these phenotypes
   */
  getPhenotypeNames() {
/*
     if ("undefined" == typeof(this._phenotypeNames)) {
      r = new Array();
      //for(let p=0,l=this.phenotypes.length;i<l;i++) {
      //  r = r.concat(this.phenotypes[i].compounds);
      //}
      r = this.phenotypes;
      this._phenotypeNames=r;
     }    
     return(this._phenotypeNames);
*/
     return(this.phenotypes);
  }

}

class PhenotypeGroupList {

  constructor(n) {
    this.listname=n; // string
    this.names=new Array(); // strings
    this._compounds=undefined; // objects
    this._phenotypes=undefined; // objects
    this._phenotypeNames=undefined; // objects
    this.groups={}; // strings -> objects
  }

  addGroup(gObj) {
    console.log(gObj);
    if (-1 == this.names.indexOf(gObj.name)) {
      this.names.push(gObj.name);
      this.groups[gObj.name] = gObj;
    }
  }

  listCompounds() {
    if ("undefined" == typeof(this._compounds)) {
      let r = new Array();
      phens = this.listPhenotypes();
      for(let i=0,l=phens.length;i<l;i++) {
        r = r.concat(phens[i].compounds);
      }
      this._compounds=r;
    }
    return(this._compounds);
  }

  listPhenotypes() {
    console.log("listPhenotypes");
    if ("undefined" == typeof(this._phenotypes)) {
      console.log("recreating");
      //console.log(this.groups);
      let r = new Array();
      for(let g in this.groups) {
        console.log(g);
        let phens=this.groups[g].phenotypes;
        //console.log(phens);
        r = r.concat(phens); // fixme - this concat seems wrong
      }
      this._phenotypes=r;
    }
    return(this._phenotypes);
  }

  listPhenotypeNames() {
    console.log("listPhenotypeNames");
    if ("undefined" == typeof(this._phenotypeNames)) {
      console.log("recreating");
      //let phens = this.listPhenotypes();
      //console.log(phens);
      let r = new Array();
      for(let g in this.groups) {
        console.log(g);
        let phens=this.groups[g].phenotypes;
        for(let pName in phens) {
          console.log(pName);
          r.push(pName);
        }
      }
      this._phenotypeNames=r;
    }
    return(this._phenotypeNames);
  }


}

PhenotypeGroup.all=new PhenotypeGroupList("all");


class Phenotype {

  constructor(n,g) {
    this.id=Phenotype.number;
    this.name=n; // string
    this.groupName=g; // string
    this.compounds = new Array(); // objects
    console.log("I: Constructor creating new phenotype ("+n+","+g+")");
    let gindex=PhenotypeGroup.all.names.indexOf(g);
    //console.log("I: gindex="+gindex);
    let gObj;
    if (-1 == gindex) {
      gObj = new PhenotypeGroup(this.groupName);
      PhenotypeGroup.all.addGroup(gObj);
    } else {
      gObj = PhenotypeGroup.all.groups[this.groupName];
    }
    this.group=gObj;
  }

  addCompound(compObj) {
    if (typeof compObj == "undefined") {
      console.log("E phenotype ("+this.name+"): addCompound: passed undefined compObj\n");
    } else {
      let found=false;
      for(let i=0,il=this.compounds.length;i<il;i++) {
        if (this.compounds[i].id==compObj.id) {
          console.log("W phenotype ("+this.name+"): addCompound: passed compObj with same ID ("+compObj.id+") - skipped\n");
          found=true;
          break;
        }
      }
      if (!found) {
        this.compounds.push(compObj);
      }
      if (typeof this.group == "undefined") {
        console.log("E phenotype ("+this.name+"): addCompound: this.group is undefined.\n");
      }
      //this.group.addCompound(compObj); // don't - the list of compounds is prepared dynamically by PhenotypeGroup::listCompounds()
    }
  }

}

Phenotype.number=0;

class PhenotypeList {

  constructor(n) {
    console.log("I: Generating new phenotype list '"+n+"'");
    this.listname=n;
    this.names=new Array(); // strings
    this.phenotypes={}; // string -> object
  }

  clear() {
    this.names=new Array();
    this.phenotypes={};
  }

  addCompound(compObj) {
    if (typeof compObj == "undefined") {
      console.log("E PhenotypeList("+this.listname+")::addCompound compObj is undefined");
      return;
    }
    for(let j=0,k=compObj.phenotypes.length;j<k;j++) {
      let phenName = compObj.phenotypes[j];
      let groupName = compObj.phenotypesGroup[j];

      if (typeof phenName == "undefined") {
        console.log("E PhenotypeList ("+this.listname+"): phenName is undefined");
	break;
      }
      if (typeof groupName == "undefined") {
        console.log("E PhenotypeList ("+this.listname+"): groupName is undefined");
	break;
      }
	      
      let phenObj;
      if (-1 == this.names.indexOf(phenName)) {
        console.log("I PhenotypeList ("+this.listname+"): Adding new phen '"+phenName+"' of group ('"+groupName+"') to phenotype list "+this.listname);
        phenObj = new Phenotype(phenName,groupName);
	Phenotype.number++;
        Phenotype.all.addPhenotype(phenObj);
      } else {
	phenObj=this.phenotypes[phenName];
      }
      let lengthPrior=phenObj.compounds.length;
      phenObj.addCompound(compObj);
      if (phenObj.compounds.length==lengthPrior) {
        console.log("E PhenotypeList("+this.listname+").addCompound: failed - length of compounds for phen "+phenName+" still at "+lengthPrior);
      } else {
        console.log("I PhenotypeList("+this.listname+").addCompound: likely success - length of compounds for phen "+phenName+" now at "+phenObj.compounds.length+" up from "+lengthPrior);
      }

      let groupObj=phenObj.group;
      groupObj.addPhenotype(phenObj); // the groupObj needs to check if that phenotype is already knownh
    }
  }

  /**
   * Add single phenotype to this list of phenotypes
   */
  addPhenotype(phenObj) {
    //console.log(JSON.stringify(phenObj));
    console.log("I PhenotypeList ("+this.listname+"): addPhenotype "+phenObj.name+" ("+phenObj.groupName+")")
    this.phenotypes[phenObj.name] = phenObj;
    this.names.push(phenObj.name);
  }

  /**
   * Add exterrnal list of phenotypes to this list of phenotypes
   */
  addPhenotypeList(phenList) {
    for(let k in phenList.phenotypes) {
      this.addPhenotype(phenList.phenotypes[k]);
    }
  }

  /**
   * Return an array of names
   */
  namesUnique() {
    let r=new Array();
    for(let k in this.phenotypes) {
      r.push(k);
    }
    return(r);
  }
}

//Phenotype.all=undefined;
Phenotype.all=new PhenotypeList("all");

exports.phenotypeRegistryGetNames=function() {
  return Phenotype.all.namesUnique();
}

exports.phenotypeName2phenotypeObj=function(nameOfPhenotype) {
  let pos = Phenotype.all.names.indexOf(nameOfPhenotype);
  if (-1 == pos) {
    return(null);   
  }
  return(Phenotype.all.phenotypes[nameOfPhenotype]);
}

exports.phenotypeName2phenotypeGroupname=function(nameOfPhenotype) {
  let o = exports.phenotypeName2phenotypeObj(nameOfPhenotype);
  if (typeof o == "undefined") {
    return("");   
  } else if (null === o) {
    return("");   
  }
  return(o.groupName);
}
// return overview as HTML string, do not write to res
exports.overview = function (req,onClickFun) {

  // this shall be optimised
  //let activePhenotypesUnique=activePhenotypes.filter((elem,pos)=>activePhenotypes.indexOf(elem)==pos);
  //let activePhenotypesUnique=activePhenotypes.filter((v,i,a)=>a.indexOf(v)===i)
  //let activePhenotypesUnique=[new Set(activePhenotypes)];

  let activePhenotypesUnique=Phenotype.all.namesUnique();

  let t="<div class=\"w3-container\">";
  t += ("<p>Observed "+activePhenotypesUnique.length+" phenotypes.</p>\n");
  t += "<p>Search for Phenotype in the input field:\n";
  t += "<input class=\"w3-input w3-border w3-padding\" type=\"text\" placeholder=\"Search for Phenotype\" id=\"myPhenotypeInput\" onkeyup=\"myPhenotypeFunction()\">\n";
  t += "<div class=\"outer\" id=\"myPhenotypeDiv\">\n";
  let ps=activePhenotypesUnique;
  ps.sort();
  for(let i=0,l=ps.length;i<l;i++) {
    let p=ps[i];
    t += "<span";
    t +=" class=\"phenotypecheckbox";
    if (1==i%2) {
      t +=" phenodd";
    } else {
      t +=" pheneven";
    }
    t += '"';
    let pNoBlanks=p.replace(/[ ;,'()]/g,"");
    //t += " id=\""+pNoBlanks+"\""; // phenotype without blanks as ID - but ont in span
    t += "><input type=\"checkbox\" class=\"phenotype phenotypealone\" name=\""+pNoBlanks+"\" onclick='"+onClickFun+"'";
    t += "/>"+p+"</span>\n";
  }
  t += "</div> <!-- outer -->\n";
  t += "</div> <!-- container -->\n";
  t += "\n";
  t += " <script>\n";
  t += "function myPhenotypeFunction() {\n";
  t += "  var input, filter, table, tr, td, i;\n";
  t += "  input = document.getElementById(\"myPhenotypeInput\");\n";
  t += "  filter = input.value.toUpperCase();\n";
  t += "  div = document.getElementById(\"myPhenotypeDiv\");\n";
  t += "  spans = div.getElementsByTagName(\"span\");\n";
  t += "  for (i = 0; i < spans.length; i++) {\n";
  t += "    input = spans[i].getElementsByTagName(\"input\")[0];\n";
  t += "    if (input) {\n"
  t += "      txtValue = input.name;\n";
  t += "      if (txtValue.toUpperCase().indexOf(filter) > -1) {\n";
  t += "        spans[i].style.display = \"\";\n";
  t += "      }\n";
  t += "      else {\n";
  t += "        spans[i].style.display = \"none\";\n";
  t += "      }\n";
  t += "    }\n";
  t += "  }\n";
  t += "}\n";
  t += "</script>\n";
  return(t);
}

exports.exportPhenotypeGroups = function(id,format) {
  if (typeof format=="undefined") {
    format="text";
  }
  let header="";
  let footer="";
  let lineSepStart="";
  let lineSepEnd="\n";
  let fieldSep="\t";
  if ("html"==format) {
    header="<html><head><title>Phenotype Groups - "+id+"</title></head><body><table>\n";
    header+="<thead><tr><th nowrap>Phenotype Group</th><th nowrap>Phenotype</th><th>Compounds</th></tr></thead><tbody>\n";
    footer="</tbody></table></body></html>";
    lineSepEnd="</td></tr>\n";
    lineSepStart="<tr><td nowrap>";
    fieldSep="</td><td nowrap>";
  }

  let r="";

  for(let pgName in PhenotypeGroup.all.groups) {
    let pg = PhenotypeGroup.all.groups[pgName];
    for(let v in pg.phenotypes) {
      r += lineSepStart + pgName;
      r += fieldSep + v;
      let p = pg.phenotypes[v];
      console.log(p);
      let cs = p.compounds;
      r += fieldSep;
      for (let i=0,il=cs.length;i<il;i++) {
         let c = cs[i];
         if (i>0) r+= ",";
         if ("html"==format) {
           r += "<a href=\"/compound/"+c.id+"/"+format+"\">"+c.id+"</a>";
         } else {
           r += c.id;
         }
      }
      r += lineSepEnd;
    }
  }
  return(header+r+footer);
}


exports.exportPhenotype = function(id,format,wholePage) {
  if (typeof format=="undefined") {
    format="text";
  }
  if (typeof wholePage=="undefined") {
    wholePage=false;
  }
  let header="";
  let theader="";
  let footer="";
  let tfooter="";
  let lineSepStart="";
  let lineSepEnd="\n";
  let fieldSep="\t";
  if ("html"==format) {
    if (wholePage) {
       header="<html><head><title>Phenotype Groups - "+id+"</title></head><body>\n";
    }
    theader="<table><thead><tr><th nowrap>Phenotype Group</th><th nowrap>Phenotype</th><th>Compounds</th></tr></thead><tbody>\n";
    tfooter="</tbody></table>";
    lineSepEnd="</td></tr>\n";
    lineSepStart="<tr><td nowrap>";
    fieldSep="</td><td nowrap>";
    if (wholePage) {
       footer += "</body></html>";
    }
  }

  let r="";

  if (id<0 || id>=Phenotype.all.length) {
    r = header+"Invalid identifier: "+id+footer;
    return(r)
  }

  let p=Phenotype.all[id];
  if ("json"==format) {
    r = JSON.stringify(p);
  } else {
    if ("html"==format) {
        r = "<h1>"+id+"</h1>\n";
    } else {
        r = id+lineSepEnd;
    }
    r += theader;
    let cs = p.compounds;
    r += fieldSep;
    for (let i=0,il=cs.length;i<il;i++) {
         let c = cs[i];
         if (i>0) r+= ",";
         if ("html"==format) {
           r += "<a href=\"/compound/"+c.id+"/"+format+"\">"+c.id+"</a>";
         } else {
           r += c.id;
         }
         r += lineSepEnd;
     }
     r += tfooter;
  }
  return(header+r+footer);
}

exports.overviewGroups = function(req,onClickFun) {
  let t="<div class=\"w3-container\">";
  t +=("<p>Observed "+PhenotypeGroup.all.names.length+" phenotype groups.</p>\n");
  t += "<p>Search for Phenotype Group in the input field:\n";
  t += "<input class=\"w3-input w3-border w3-padding\" type=\"text\" placeholder=\"Search for Phenotype Group\" id=\"myPhenotypeGroupInput\" onkeyup=\"myPhenotypeGroupFunction()\">";
  t += "</p>\n";
  t += "<table id=\"myPhenotypeGroupTable\">\n";
  t += "<thead>";
  //t += "<tr bgcolor=\"orange\"><th>Phen Group</th><th>Phenotypes</th></tr>\n";
  t += "<tr bgcolor=\"#ffa500\"><th>Phen Group</th><th>Phenotypes</th></tr>\n";
  t += "</thead>\n";
  t += "<tbody>";
  let lineNo=0;
  for(let pgName in PhenotypeGroup.all.groups) {
    lineNo++;
    let pg = PhenotypeGroup.all.groups[pgName];
    t += "<tr";
    t += " bgcolor=\""+modDecoration.phenotypeGroupname2colour(pgName);
    t += "\"><td>"+pgName+"</td>";
    t += "<td>";
    t += "<div class=\"outer\">\n";
    let i=0;
    for(let v in pg.phenotypes) {
      i++;
      let vNoBlanks=v.replace(/[ ;,'()<>&]/g,"");
      t += "<span";
      if (1==i%2) {
        t +=" class=\"phengroupsecond\"";
      } else {
        t +=" class=\"phengroupfirst\"";
      }
      t += "><input type=\"checkbox\" class=\"phenotype phenotypeingroup\" name=\""+vNoBlanks+"\" onclick='"+onClickFun+"'";
      t += "/>"+v+"</span>\n";
    }
    t += "</div>\n";
    t += "</td></tr>\n";
  }
  t += "</tbody></table>\n";
  t += "</div>";
  t += "<script>\n";
  t += "function myPhenotypeGroupFunction() {",
  t += "  var input, filter, table, tr, td, i;";
  t += "  input = document.getElementById(\"myPhenotypeGroupInput\");";
  t += "  filter = input.value.toUpperCase();";
  t += "  table = document.getElementById(\"myPhenotypeGroupTable\");";
  t += "  tr = table.getElementsByTagName(\"tr\");";
  t += "  for (i = 0; i < tr.length; i++) {";
  t += "    td = tr[i].getElementsByTagName(\"td\")[0];";
  t += "    if (td) { txtValue = td.textContent || td.innerText;";
  t += "      if (txtValue.toUpperCase().indexOf(filter) > -1) {";
  t += "        tr[i].style.display = \"\";\n";
  t += "      }";
  t += "      else {";
  t += "        tr[i].style.display = \"none\";";
  t += "      }";
  t += "    }";
  t += "  }";
  t += "}";
  t += "</script>\n";
  return(t);
}

exports.all = Phenotype.all;
exports.phenotypegroups = PhenotypeGroup.all;
