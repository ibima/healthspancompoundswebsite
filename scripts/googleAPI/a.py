#!/usr/bin/python
from __future__ import print_function

from pprint import pprint
from googleapiclient import discovery
from time import sleep

# TODO: Change placeholder below to generate authentication credentials. See
# https://developers.google.com/sheets/quickstart/python#step_3_set_up_the_sample
#
# Authorize using one of the following scopes:
#     'https://www.googleapis.com/auth/drive'
#     'https://www.googleapis.com/auth/drive.file'
#     'https://www.googleapis.com/auth/drive.readonly'
#     'https://www.googleapis.com/auth/spreadsheets'
#     'https://www.googleapis.com/auth/spreadsheets.readonly'
credentials = None

service = discovery.build('sheets', 'v4', credentials=credentials)

# The spreadsheet to request.
# Steffen's stupid test with two sheets
#spreadsheet_id = '1aB5vo1npcfSGsUdo8PdGx_EhYjnKk66Dqw388_979qw'  # TODO: Update placeholder value.
# Nadine's database

spreadsheetIDs = {
  "A": "1Q7hUv1lyBg0QwjrptM3pAvxcPpUvkNLWAJsLwgsIA7I",
  "B": "1PBET3n4VbRbBkDDHRMp5_5R5uncY3XdX_XsASK2rHLU",
  "C": "1ZLzY2d2UiA3kJYkEu0qhQtlUA8SkAzPTWfZzfk6f4wQ",
  "D": "1VZS0dFV4_-Gwm05bcYcebP-ljgbpfXJN-ToxQJ8TxWk",
  "E": "19Jid0F4dihzE_8bdvig6WxeLd4L5MfEnvRgrN24_x9o",
}

i=0
for filename in spreadsheetIDs:

   spreadsheet_id = spreadsheetIDs[filename]

   request = None
   request = service.spreadsheets().get(spreadsheetId=spreadsheet_id, includeGridData=False)
   response = None
   response = request.execute()

   rangesArray=[]
   j=0
   for sheet in response["sheets"]:
      sheetId=sheet["properties"]["sheetId"]
      sheetTitle=sheet["properties"]["title"]
      #print(sheetId, " : ", sheetTitle)
      range=sheetTitle+"!"+"B2"
      rangesArray.append(range)

   request2 = None
   request2 = service.spreadsheets().values().batchGet(spreadsheetId=spreadsheet_id, ranges=rangesArray)
   response2 = None
   response2 = request2.execute()
   print("I: sleeping 5 seconds - avoiding trouble with Google")
   sleep(5)

   for r in response2["valueRanges"]:
      n = r["range"].replace("!B2","")
      v = "N/A"
      if r.get("values") is not None:
         v = r["values"][0][0]
      print(i,filename,j,n,v)
      i = i+1
      j = j+1

