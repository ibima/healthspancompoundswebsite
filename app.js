/**
 * Node.js - provided web interface
 *
 * Author: Steffen Möller
 *         2018-2020
 */

const http = require('http');
const path = require('path');
const express = require('express');
const fs = require('fs');
const url = require('url');
const modCompound = require('./modCompound');
const modStrains = require('./modStrains');
const modPhenotypes = require('./modPhenotypes');
const modDecoration = require('./modDecoration');

//const hostname = 'natural.functional.domains';
//const hostname = '127.0.0.1';
const hostname = 'www.healthy-worm-database.eu';
const port = process.env.PORT || 3000;

const dataDir = "data";
const googledataMappingFilename="googledata_mapping.tsv";


var app=express();

var publicdir = path.join(__dirname, 'public');
app.use(express.static(publicdir));

//app.use(express.json());

// read data directory once at startup
var dataDirContent = fs.readdirSync(dataDir); // var, not const, to allow for online updates
var orig = new Array();
var compounds = new Array();

var googledataMapping = new Object();

if (fs.existsSync(googledataMappingFilename)) {
   console.log("I app init googledataMapping found: "+googledataMappingFilename)
   let f = fs.openSync(googledataMappingFilename,"r");
   let mapping = fs.readFileSync(f,"utf8"); //.split("\n");
   //console.log("I app init mapping: "+mapping)
   fs.closeSync(f);
   let lines = mapping.split("\n");
   if (lines[0].endsWith("\r")) {
      lines = mapping.split("\r\n");
   }
   for (let i=0,il=lines.length; i<il; i++) {
      let l=lines[i];
      if (typeof l == "undefined") continue;
      let s=l.split("\t");
      if (typeof s == "undefined") continue;
      let sfile=s[0];
      if (typeof sfile == "undefined") continue;
      if (sfile == "") continue;
      googledataMapping[sfile]=s;
      //console.log("I app init mapping i="+i+": "+sfile+"->"+googledataMapping[sfile]);
   }
   console.log("I app init googledataMapping:");
   console.log(googledataMapping);
} else {
   console.log("E app init: Could not find file '"+googledataMappingFilename+"'");
}


for(var i=0,l=dataDirContent.length;i<l;i++) {
  if (".git"==dataDirContent[i] || "README.md"==dataDirContent[i]) {
    continue;
  }
  let filename=dataDirContent[i]
  let v=modCompound.readCompoundFile(dataDir,filename);
  let vMap = googledataMapping[filename];
  if (typeof vMap == "undefined") {
    console.log("I app init: could not retrieve URL for '"+filename+"'"); } else {
    v.url="https://docs.google.com/spreadsheets/d/"+vMap[3]+"/edit#gid="+vMap[2]
    v.url="https://docs.google.com/spreadsheets/d/"+vMap[3]+"/edit#gid="+vMap[2]
    console.log("I app init: set URL to "+v.url+" for '"+filename+"'");
  }
  if (typeof v == "undefined") {
    console.log("E app init: modCompound.readCompoundFile returned undefined for "+dataDirContent[i]);
    continue;
  }
  //console.log(v);
  //console.log(v.name);
  //console.log(v.phenotypes);
  orig.push(v);
  let simple=v.simplify();
  if (typeof simple == "undefined") {
    console.log("E app init: modCompound.simplify returned undefined for "+dataDirContent[i]);
    continue;
  }
  if (typeof simple.url == "undefined") {
    console.log("W app init: modCompound.simplify lost URL for "+dataDirContent[i]);
  }
  //console.log(simple);
  //console.log(simple.name);
  //console.log(simple.phenotypes);
  compounds.push(simple);
}
console.log("Read "+compounds.length+" compounds from "+dataDir+" directory.");

for(let i=0,l=compounds.length;i<l;i++) {
  let c = compounds[i];
  if (typeof c == "unknown") {
    console.log("E app init: position "+i+" in compounds array undefined");
    break;
  }
  modStrains.strainRegistryAddCompound(compounds[i]);
}
//console.log("Read the following strains:");
//console.log(JSON.stringify(modStrains.strainRegistryGetNames()));

//
for(let i=0,l=compounds.length;i<l;i++) {
  if (typeof c == "unknown") {
    console.log("E app init: position "+i+" in compounds array undefined");
    break;
  }
  // accept all compounds if no selection made
  modPhenotypes.all.addCompound(compounds[i]);
}

/*
const favicon = new Buffer.from('AAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAA/4QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEREQAAAAAAEAAAEAAAAAEAAAABAAAAEAAAAAAQAAAQAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAEAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA//8AAP//AAD8HwAA++8AAPf3AADv+wAA7/sAAP//AAD//wAA+98AAP//AAD//wAA//8AAP//AAD//wAA', 'base64');
app.get("/favicon.ico", function(req, res) {
  res.statusCode = 200;
  res.setHeader('Content-Length', favicon.length);
  res.setHeader('Content-Type', 'image/x-icon');
  res.setHeader("Cache-Control", "public, max-age=2592000");                // expiers after a month
  res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
  res.end(favicon);
});
*/

app.get('/orig/:id?', (req,res) => {
  res.setHeader('Content-Type', 'application/json; charset=utf-8');
  res.statusCode = 0;
  let ids=req.params.id.split(",");

  if (typeof ids == 'undefined') {
    res.statusCode = 404;
    res.send("Error: Please specify the compound IDs to retrieve.'\n");
    res.end();
    return;
  }

  // initial check if all compound IDs are numeric
  for (let i=0,l=ids.length; i<l; i++) {
    let id=parseInt(ids[i]);
    if(isNaN(id)) {
      res.statusCode = 404;
      res.send("Error: Not a number: '"+id+"'\n");
    } else if (id < 0 || id >= orig.length) {
      res.statusCode = 404;
      res.send("Error: Cannot provide compound #"+id+"\n");
    }
  }

  if (404 == res.statusCode) {
    res.send("Error: Cannot provide data for '"+req.params.id+"'\n");
    res.end();
    return;
  }

  res.statusCode = 200;

  // always returning an array of compounds - needs to be reconsidered
  res.write("[\n");
  for (let i=0,l=ids.length; i<l; i++) {
    let id=parseInt(ids[i]);

    if(isNaN(id)) {
      // redudant check, should not be reached - ignoring ID
      continue;
    }

    if (id<0 || id >= orig.length) {
      // redudant check, should not be reached - ignoring ID
      continue;
    }
    if (0<i) res.write(",\n");
    res.write(JSON.stringify(orig[id]));
  }
  res.write("]\n");
  res.end();
});

//
  //res.setHeader('Content-Type', 'application/json; charset=utf-8');
app.get('/strain/:id?', (req,res) => {
  res.setHeader('Content-Type', 'text/text; charset=utf-8');
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.statusCode = 200;
  let fieldSep="\t";
  let startRow="";
  let endRow="\n";
  if (typeof id != "undefined") {
    let ids=req.params.id.split(",");
    res.write("Selection for strains by their numerical IDs is not prepared. Shout out that you need it.\n");
  } else {
    let n = modStrains.strainRegistryGetNames();
    for(let i=0,l=n.length; i<l; i++) {
      let strainName=n[i];
      res.write(startRow);
      res.write(n[i]);
      res.write(fieldSep);
      let cs = modStrains.strainRegistryName2Compounds(strainName);
      console.log("strainName: " +strainName + "\tcs:" + cs);
      if (typeof cs == "undefined") {
        res.write("W: strain '"+strainName+"' not found in internal reference\n");
        console.log("E: strain '"+strainName+"' not found in internal reference\n");
      } else {
        for (let j=0,k=cs.length; j<k; j++) {
          if (typeof cs[j] == "undefined") {
            console.log("E strain: have undefined compound ID in strain registry for strain '"+strainName+"' as position "+j+"\n");
          } else {
            let compoundName = compounds[cs[j]].name;
            v = cs[j] + "("+compoundName+")"
            if (0 == j) {
              res.write(v);  // res.write does not want to write numbers
            } else {
              res.write(","+v);
            }
          }
        }
      }
      res.write(endRow);
    }
  }
  res.end();
});

app.get('/phenotypegroup/:id?/:format?', (req,res) => {
  let format=req.params.format;
  let id=req.params.format;
  if (typeof format=="undefined") {
    console.log("I /phenotypegroup/: format undefined, set to text");
    format="text";
  }
  if (typeof id=="undefined") {
    console.log("I /phenotypegroup/: id undefined, set to all");
    format="all";
  }
  if ("html"==format || "text"==format) {
    res.setHeader('Content-Type', 'text/'+format+'; charset=utf-8');
  }
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.write(modPhenotypes.exportPhenotypeGroups(id,format));
  res.end();
});

app.get('/flat/:id?/:format?', (req,res) => {
  let attribute=req.params.id;
  let format=req.params.format;
  if (typeof attribute=="undefined") {
    console.log("E init: attribute passed was unknown - "+req.params);
    res.write("E init: attribute passed was unknown\n");
    res.write(req.params);
    res.end();
    return;
  }
  if (typeof format=="undefined") {
    format="dot";
  }
  switch(attribute) {
    case "0": attribute=0; break;
    case "1": attribute=1; break;
    case "2": attribute=2; break;
    case "3": attribute=3; break;
    case "all": attribute=undefined; break;
    default:
      console.log("E init: invalid attribute passed - "+req.params);
      res.write("E init: invalid attribute passed\n");
      res.write(req.params);
      res.end();
      return;
  }
  res.setHeader('Content-Type', 'text/text; charset=utf-8');
  res.setHeader("Access-Control-Allow-Origin", "*");
  let printHeader=true;
  for (let i=0,il=compounds.length; i<il; i++) {
    let c=compounds[i];
    res.write(c.exportFlat(modPhenotypes.phenotypegroups,attribute,format,printHeader));
    printHeader=false;
  }
  res.end();
});

app.get('/phenotype/', (req,res) => {
  let format=req.param.format;
  let id=req.param.id;

  if (typeof format == "undefined" && typeof id == "undefined") {
    format="text";
    id="all";
  }
  else if (typeof format == "undefined") {
    format="text";
  }

  if (typeof id == "undefined") {
    id="all";
  } else if ("json"==id || "text"==id || "html"==id) {
    format=id
    id="all";
  }

  res.statusCode = 200;
  if("json"==format) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.setHeader("Access-Control-Allow-Origin", "*");
  } else if ("text"==format) {
    res.setHeader('Content-Type', 'text/text; charset=utf-8');
    res.setHeader("Access-Control-Allow-Origin", "*");
  } else if ("html"==format) {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.setHeader("Access-Control-Allow-Origin", "*");
  } else {
    res.statusCode = 404;
    res.send("Error: Please specify either json or text as format.'\n");
    res.end();
    return;
  }

  res.write("format: "+format);

  if ("json"==format) {
      if ("all"==id) {
      } else {
      }
  } else if ("html"==format){
      if ("all"==id) {
        for(let phenid=0,k=activePhenotypesUnique.length;phenid<k;phenid++) {
          let phen=activePhenotypesUnique[phenid];
          textPhenotypesId.push(phenid);
        }
      } else {
      }
  } else {
       let ids=req.params.id.split(",");
       let n = modPhenotypes.phenotypeRegistryGetNames();
       for(let i=0,l=n.length; i<l; i++) {
         res.write(n[i]);
         res.write("\n");
       }
  }
  res.end();

});

app.get('/notes/:id?', (req,res) => {
  res.statusCode = 0;
  //let ids=req.params.id.split(",");
  let ids="all";
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  res.setHeader("Access-Control-Allow-Origin", "*");

  if (typeof ids == 'undefined') {
    res.statusCode = 404;
    res.send("Error: Please specify the compound IDs to retrieve, or 'all' for all.\n");
    res.end();
    return;
  }

  if ("all"==ids) {
    ids = new Array();
    for(let i=0,l=compounds.length;i<l;i++) {
      ids.push(i);
    }
  } else {
    // initial check if all compound IDs are numeric
    for (let i=0,l=ids.length; i<l; i++) {
      let id=parseInt(ids[i]);
      if(isNaN(id)) {
        res.statusCode = 404;
        res.send("Error: Not a number: '"+id+"'\n");
      } else if (id<0 || id >= compounds.length) {
        res.statusCode = 404;
        res.send("Error: Cannot provide compound #"+id+"\n");
      }
    }
  }
  res.statusCode = 200;
  for(const id in ids) {
    let c=compounds[id];
    // write fields describing conditions
    let phenotypePos=9;
      res.write(c.name);
    for(let i=0,l=c.phenotypes.length;i<l;i++) {
      let pname=c.phenotypes[i];
      let pgroup=c.phenotypesGroup[i];
      let tk=c.rows[1];
      let tl=c.rows[2];
      for(let j=9,jl=tl.fields.length;j<jl;j++) {
         if ("notes"==tl.fields[j]) {
            let phen=tk.fields[j-3];
	for(r=3;r<c.nlines;r++) {
	res.write(c.name+"\t"+phen+"\t"+c.rows[r].fields[j]+"\n");
	}
            j += 3; // +1 in for loop
         }
      }
    }
    res.write("\n"); // end of record - optics only
  }
  res.end();
});

app.get('/conditions/:id', (req,res) => {
  //let ids=req.params.id.split(",");
  let ids="all";
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  res.setHeader("Access-Control-Allow-Origin", "*");

  if (typeof ids == 'undefined') {
    res.statusCode = 404;
    res.send("Error: Please specify the compound IDs to retrieve, or 'all' for all.\n");
    res.end();
    return;
  }

  if ("all"==ids) {
    ids = new Array();
    for(let i=0,l=compounds.length;i<l;i++) {
      ids.push(i);
    }
  } else {
    // initial check if all compound IDs are numeric
    for (let i=0,l=ids.length; i<l; i++) {
      let id=parseInt(ids[i]);
      if(isNaN(id)) {
        res.statusCode = 404;
        res.send("Error: Not a number: '"+id+"'\n");
      } else if (id<0 || id >= compounds.length) {
        res.statusCode = 404;
        res.send("Error: Cannot provide compound #"+id+"\n");
      }
    }
  }
  res.statusCode = 200;
  for(let i=0,l=ids.length;i<l;i++) {
    let id=ids[i];
    let c=compounds[id];
    // write fields describing conditions
    for(let n=(0==i?2:3),nl=c.nlines;n<nl;n++) {
      let tl=c.rows[n];
      if (2>=n) {
	res.write("compound");
      } else {
        res.write(c.name);
      }
      for(let f=0;f<8;f++) {
        res.write("\t");
        res.write(tl.fields[f]);
      }
      res.write("\n"); // end of line
    }
    res.write("\n"); // end of record - optics only
  }
  res.end();
});

app.get('/form', (req,res) => {
    let format=req.query["format"];
    if (typeof format === "undefined") {
      format="html";
    }
    if ("submit"==format||"Submit"==format) {
      format="html";
    }
    if ("html"==format) {
      res.setHeader('Content-Type', 'text/html; charset=utf-8');
      res.write("<html><head>\n");
      res.write("  <title>Details on selected compounds</title>\n");
      res.write("</head>\n<body>\n");
    }

    for (const key in req.query) {
      console.log(key, req.query[key])
    }

    let ids=req.query["id"];
    if (typeof ids === "undefined") {
       //res.statusCode = 404;
       res.write("<h1><b>No compound selected!</b></h1>Please specify the compound IDs (currently undefined). If you are not a machine using this site then select one or multiple of the checkboxes of the page that brought you here.\n");
       res.write("</body></html>\n");
       res.end();
       return;
    }
    if (true) {
      res.write("<pre>");
      res.write(JSON.stringify(ids))
      res.write("</pre>");
    }
    for (let i=0,l=ids.length; i<l; i++) {
      let id=parseInt(ids[i]);
      if(isNaN(id)) {
        res.statusCode = 404;
        res.send("Error: Not a number: '"+id+"'\n");
      } else if (id<0 || id >= compounds.length) {
        res.statusCode = 404;
        res.send("Error: Cannot provide compound #"+id+", please 0 &lt;= id &lt;= "+(compounds.length - 1)+"\n");
      }
    }

    res.statusCode = 200;
    for (let i=0,l=ids.length; i<l; i++) {
      let id=parseInt(ids[i]);
      if(isNaN(id)) {
        res.statusCode = 404;
        res.send("Error: Not a number: '"+id+"' - weird, was tested before.\n");
      }
      res.write("Compound ID "+id);
      let c=compounds[id];
      if (typeof c === "undefined") {
        res.write("compound[id]  -  undefined");
        console.log("compound[id]  -  undefined");
      } else {
        res.write("\n");
	res.write("<p>Compound ID "+id+": "+c.id+" "+c.name+"</p>\n");
	console.log("<p>Compound ID "+id+": "+c.id+" "+c.name+"</p>\n");
        res.write(c.write(format));
        res.write("\n");
      }
    }
    res.write("</body></html>\n");
    res.end();
});

app.get('/compound/:id/:format?', (req,res) => {
  let format=req.params.format;
  if (typeof format == "undefined") {
     format="html";
  } else if ("submit"==format||"Submit"==format) {
    format="html";
  }
  let ids=req.params.id.split(",");
  if ("text"==format) {
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  } else if ("json"==format) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
  } else if ("html"==format) {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
  }
  res.setHeader("Access-Control-Allow-Origin", "*");
  //res.write(JSON.stringify(req.params));
  //res.write(JSON.stringify(ids));
  
  if ("html"==format) {
    res.write("<html><head>\n");
  }
  if (typeof ids == 'undefined') {
    res.statusCode = 404;
    res.send("Error: Please specify the compound IDs to retrieve, or 'all' for all.\n");
    res.end();
    return;
  }

  if ("all"==ids) {
    ids = new Array();
    for(let i=0,l=compounds.length;i<l;i++) {
      ids.push(i);
    }
  } else {
    // initial check if all compound IDs are numeric
    for (let i=0,l=ids.length; i<l; i++) {
      let id=parseInt(ids[i]);
      if(isNaN(id)) {
        res.statusCode = 404;
        res.send("Error: Not a number: '"+id+"'\n");
      } else if (id<0 || id >= compounds.length) {
        res.statusCode = 404;
        res.send("Error: Cannot provide compound #"+id+"\n");
      }
    }
  }

  res.statusCode = 200;
  //res.write("start\n");
  if ("html"==format) {
    res.write("</head><body>\n");
  }
  else if ("json"==format) {
    res.write("[\n");
  }
  for(let i=0,l=ids.length; i<l; i++) {
    let id=ids[i];
    if ("html"==format || "text"==format || "tsv"==format) {
      res.write("Compound ID "+id);
    }
    let c=compounds[id];
    if ("html"==format || "text"==format || "tsv"==format) {
      if (typeof c === "undefined") {
        res.write("  -  undefined");
      }
      res.write("\n");
      res.write(compounds[id].write(format));
    }
    else if ("json"==format) {
      if (0<i) res.write(",\n");
      res.write(JSON.stringify(compounds[id]));
    }
  }
  //res.write("end\n");
  if ("html"==format) {
    res.write("</body></html>\n");
  }
  else if ("json"==format) {
    res.write("]\n");
  }
  res.end();
});

app.get("/", (req,res) => {
  res.setHeader('Content-Type', 'text/html');
  res.write("<!DOCTYPE html>\n");
  res.write("<html>\n<head>\n<title>Compounds with reported effects on Healthspan</title>\n");
  res.write("<meta charset=\"UTF-8\" />\n");
  res.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
  res.write("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">\n");
  res.write("<link rel=\"icon\" href=\"/favicon.ico\" type=\"image/x-icon\">\n");
  res.write("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js\"></script>\n");
  res.write("<style>\n");
// res.write("body {>\n");
// res.write("  background-image:url(header_worm.jpg);\n");
// res.write("}>\n");
  res.write("div.imgheader {\n");
  res.write("  background-size: cover;\n");
  res.write("  width: 100%;\n");
  res.write("  height: 50;\n");
  res.write("  position: relative;\n");
  res.write("}\n");
  res.write("div.outer {\n");
  res.write("  display:inline-block;\n");
  res.write("}\n");
  res.write("h1 {\n");
  res.write("  color: darkred;\n");
  res.write("  text-align: center;\n");
  res.write("  font-weight: bold;\n");
  res.write("  font-size: 500%;\n");
  res.write("  background: lightyellow;\n");
  res.write("}\n");
  res.write("span.phengroupfirst {\n");
  res.write("  background: white;\n");
  res.write("}\n");
  res.write("span.phengroupsecond {\n");
  res.write("  background: lightyellow;\n");
  res.write("}\n");
  res.write("span.pheneven {\n");
  res.write("  background: white;\n");
  res.write("}\n");
  res.write("span.phenodd {\n");
  res.write("  background: lightblue;\n");
  res.write("}\n");
  res.write("p.selector {\n");
  res.write("  background: lightgray;\n");
  res.write("}\n");
  res.write("tr.compoundeven {\n");
  res.write("  background: lightyellow;\n");
  res.write("  padding-top: 0px;\n");
  res.write("  padding-bottom: 0px;\n");
  res.write("  margin-top: 0px;\n");
  res.write("  margin-bottom: 0px;\n");
  res.write("  outline-width: 0px;\n");
  res.write("  outline-style: none;\n");
  res.write("}\n");
  res.write("tr.compoundodd {\n");
  res.write("  background: white;\n");
  res.write("  padding-top: 0px;\n");
  res.write("  padding-bottom: 0px;\n");
  res.write("  margin-top: 0px;\n");
  res.write("  margin-bottom: 0px;\n");
  res.write("  outline-width: 0px;\n");
  res.write("  outline-style: none;\n");
  res.write("}\n");
  res.write("span.straineven {\n");
  res.write("  background: lightgreen;\n");
  res.write("}\n");
  res.write("span.strainodd {\n");
  res.write("  background: white;\n");
  res.write("}\n");
  res.write("th.phenotypeheader {\n");
//res.write("  background: yellow;\n");
  res.write("  writing-mode: tb-rl;\n");
  res.write("}\n");
  res.write("table {\n");
  res.write("  padding-right: 2px;\n");
  res.write("  border: 2px;\n");
  res.write("}\n");
  res.write("td, th, tr {\n");
  res.write("  padding-top: 0px;\n");
  res.write("  padding-bottom: 0px;\n");
  res.write("  margin-top: 0px;\n");
  res.write("  margin-bottom: 0px;\n");
  res.write("  outline-width: 0px;\n");
  res.write("  outline-style: none;\n");
  res.write("  border-collapse: collapse;\n");
  res.write("  border: 0px;\n");
  res.write("  border-spacing: 0px;\n");
  res.write("}\n");
  res.write("td.phenWithContent {\n");
  res.write("  text-align: center;\n");
//  res.write("  background: lightgreen;\n"); // individually adjusted to phenotype group
  res.write("}\n");
  res.write("td.phenWithNoContent {\n");
  res.write("  background: white;\n");
  res.write("}\n");
  res.write("</style>\n");

  res.write("<script>\n");
/*
     +'      $("td.compound[data-phenotypes*=\""+phen+"\"]").show();\n'
     +'      $("th.phenotypeheader[data-phenotypes=\""+phen+"\"]").show();\n'
 //  +'      $("tr."+id).css("border-spacing","5px");\n'
     // filling gaps
     +' $("th.phenotypeheader:visible").each(function(){\n'
     +'  let phenid=$(this).data("phenid");\n'
     +'  let phenname=$(this).data("phenotypes");\n'
     +'  console.log("phen="+phenname+" ("+phenid+")");\n'
     +'  $("td.compoundheader:visible").each(function(){\n'
     +'    let compoundid=$(this).data("compoundid");\n'
     +'    console.log("phen="+phenid+": compoundid="+compoundid);\n'
     +'    $("td.compound[data-compoundid="+compoundid+"][data-phenotypes="+phenname+"]").show();\n'
     +'  });\n'
*/

  res.write("function myCompoundFunction() {\n");
  res.write("  var input, filter, table, tr, td, i;\n");
  res.write("  input = document.getElementById(\"myCompoundInput\");\n");
  res.write("  table = document.getElementById(\"compoundsOverview\");\n");
  res.write("  filter = input.value.toUpperCase();\n");
  res.write("  rows = table.getElementsByTagName(\"tr\");\n");
  res.write("  $(\"p.messages\").append(\"myCompoundFunction: \" + rows.length);\n");
  res.write("  for (i = 0; i < rows.length; i++) {\n");
  res.write("    td = rows[i].getElementsByTagName(\"td\")[1];\n");
  //res.write("    $(\"p.messages\").append((\"myCompoundFunction: \" + rows.length + \" -- \" + input));\n");
  res.write("    if (td) {\n");
  res.write("      txtValue = td.innerHTML;\n");
  res.write("      if (txtValue.toUpperCase().indexOf(filter) > -1) {\n");
  res.write("        rows[i].style.display = \"\";\n");
  res.write("      }\n");
  res.write("      else {\n");
  res.write("        rows[i].style.display = \"none\";\n");
  res.write("      }\n");
  res.write("    } else {\n");
  //res.write("  $(\"p.messages\").append(\"myCompoundFunction: \" + rows+\"(\"+i+\") empty\");\n");
  res.write("    }\n");
  res.write("  }\n");
  res.write("}\n");


  res.write("function mySelectionHandler() {\n");
  res.write("  $(\"p.messages\").innerHTML=\"mySelectionHandler called\";\n");
  res.write("  console.log(\"mySelectionHandler called.\");\n");
  res.write("  let inputPhens = document.getElementById(\"selectedPhenotypes\");\n");
  res.write("  let inputStrains = document.getElementById(\"selectedStrains\");\n");
  res.write("  var selPhens=inputPhens.value;\n");
  res.write("  var selStrains=inputStrains.value;\n");
  res.write("  console.log(\"mySelStrainStrains Phens \"+selPhens);\n");
  res.write("  console.log(\"mySelectionHandler Strains \"+selStrains);\n");
  res.write('  let overviewTable=document.getElementById("compoundsOverview");\n');
  res.write('  let selStrainsSplitByComma = selStrains.split(","); // Typically, only a few strains are selected\n');
  res.write('  let selPhenotypesSplitByComma = selPhens.split(","); // Typically, only a few phenotypes are selected\n');
  res.write('  if ("" == selPhens && "" == selStrains) {');
  res.write("     $(\"p.messages\").append(\" Nothing selected - show all\");\n");
  res.write("     console.log(\" Nothing selected - show all\");\n");
  res.write('     $("th.phenotypeheader").show()\n');
  res.write('     $("th.compound").show()\n');
  res.write('     $("td.compound").show()\n');
  res.write('     for (let i=1, row; row = overviewTable.rows[i]; i++) {\n');
  res.write('       row.hidden=false;');
  res.write('     }\n');
  res.write('  } else if ("" == selPhens) {\n');
  res.write('     $("p.messages").append(\" No phenotypes selected but strains are "+selStrains);\n');
  res.write('     $("th.compound").show()\n');
//  res.write('     $("th.phenotypeheader").hide()\n');
  res.write('     $("td.compound").show()\n');
  res.write('     console.log(selStrainsSplitByComma);\n');
  res.write('     for (let i=1, row; row = overviewTable.rows[i]; i++) {\n');
  res.write('       let visible=false;\n');
  res.write('       let strainRow=row.cells[3].innerText; // This may list many strains\n');
//  res.write('       console.log(strainRow + " vs " + selStrains);\n');
  res.write('       for (let strainNo=0, strainSelLength=selStrainsSplitByComma.length; strainNo<strainSelLength; strainNo++) {\n');
  res.write('         let strainQuery = selStrainsSplitByComma[strainNo].trim();\n');
  res.write('         if (strainRow.indexOf(strainQuery) > -1) {\n');
//  res.write('           console.log(strainRow + " vs " + strainQuery);\n');
  res.write('           visible = true;\n');
//  res.write('           console.log("Found");\n');
  res.write('           break; // "or" - no need to check other strains\n');
  res.write('         }\n');
  res.write('       }\n');
  res.write('       if (visible) {\n');
//  res.write('         row.hidden=false;\n');
  res.write('         console.log("found");\n');
  res.write('       } else {\n');
//  res.write('         console.log("hiding");\n');
  res.write('         row.hidden=true;\n');
  res.write('       }\n');
  res.write('     }  // iteration over selected strains\n');
  res.write('  } else if ("" == selStrains) {\n');
  res.write('     $("p.messages").append(" No strains selected but phenotypes are: "+selPhens);\n');
//  res.write('     let overviewTableHeader=overviewTable.rows[0];\n');
/* // Always show all columns
  res.write('     for (let i=0,th; th=overviewTableHeader.cells[i]; i++) {\n');
  res.write('       myText = th.innerText.trim();\n');
  res.write('       myTextNoBlanks = myText.replace(RegExp(" ","g"),"");\n');
  res.write('       console.log(index + ": " + myText + "<->" + myTextNoBlanks + "<->" + selPhens);\n');
  res.write("       if (4 >= index || selPhens.indexOf(myText) > -1 || selPhens.indexOf(myTextNoBlanks) > -1) {\n");
  res.write('         console.log(i+": th.style.display=");\n');
  res.write('         th.style.display="";\n');
  res.write('       } else {\n');
  res.write('         console.log(i+": th.style.display=none");\n');
  res.write('         th.style.display="none";\n');
  res.write('       }\n');
  res.write('     }\n // for');
*/

/*
  res.write('     for (let i=1, row; row = overviewTable.rows[i]; i++) {\n');
  res.write('       for (let j = 0, col; col = row.cells[j]; j++) {\n');
  res.write('         col.visible = overviewTableHeader.cells[j].visible;\n');
  res.write('       }\n');
  res.write('     }\n');
*/

/*
  res.write('     $("th.phenotypeheader").each(function(index){\n');
  res.write('       myText = $(this).text().trim();\n');
  res.write('       myTextNoBlanks = myText.replace(RegExp(" ","g"),"");\n');
  res.write('       console.log(index + ": " + myText + "<->" + myTextNoBlanks + "<->" + selPhens);\n');
  res.write("       if (4 >= index || selPhens.indexOf(myText) > -1 || selPhens.indexOf(myTextNoBlanks) > -1) {\n");
//  res.write("         $(this).show();\n");
  res.write('         $("tr td:nth-child("+index+")").show();\n');
  res.write("       } else {\n");
 // res.write("         $(this).hide();\n");
  res.write('         $("tr td:nth-child("+index+")").hide();\n');
  res.write("       }\n");
  res.write("     });\n");
*/


//  res.write('     $("th.phenotypeheader").hide();\n');
 // res.write('     $("th.compound").show();\n');
  //res.write('     $("td.compound[data-phenotypes="+selPhens+"]").show();\n');
  //res.write('     $("th.phenotypeheader[data-phenotypes="+selPhens+"]").show();\n');

  res.write('     $("p.messages").append(\" No strains selected but phenotypes are "+selPhens);\n');
  res.write('     $("th.compound").show()\n');
  res.write('     $("td.compound").show()\n');
//  res.write('     $("th.phenotypeheader").hide()\n');
  res.write('     console.log(selPhenotypesSplitByComma);\n');
  res.write('     for (let i=1, row; row = overviewTable.rows[i]; i++) {\n');
  res.write('       let visible=false;\n');
  res.write('       let phenotypeRow=row.cells[0].getAttribute("data-phenotypes"); // This may list many phenotypes\n');
//  res.write('       console.log(phenotypeRow + " vs " + selPhens);\n');
  res.write('       for (let phenotypeNo=0, phenotypeSelLength=selPhenotypesSplitByComma.length; phenotypeNo<phenotypeSelLength; phenotypeNo++) {\n');
  res.write('         let phenotypeQuery = selPhenotypesSplitByComma[phenotypeNo].trim();\n');
  res.write('         if (phenotypeRow.indexOf(phenotypeQuery) > -1) {\n');
//  res.write('           console.log(phenotypeRow + " vs " + phenotypeQuery);\n');
  res.write('           visible = true;\n');
//  res.write('           console.log("Found");\n');
  res.write('           break; // "or" - no need to check other phenotypes\n');
  res.write('         }\n');
  res.write('       }\n');
  res.write('       if (visible) {\n');
  res.write('         row.hidden=false;\n');
 // res.write('         console.log("found");\n');
  res.write('       } else {\n');
  res.write('         console.log("hiding");\n');
//  res.write('         row.hidden=true;\n');
  res.write('       }\n');
  res.write('     }  // iteration over selected phenotypes\n');

  res.write('  } else {\n');
  res.write('     $("p.messages").append(" Both strains and phenotypes are selected");\n');
  res.write('     $("th.compound").show()\n');
  res.write('     //$("th.phenotypeheader").hide()\n');
  res.write('     //$("td.compound").hide()\n');
  res.write('     console.log(selPhenotypesSplitByComma);\n');
  res.write('     for (let i=1, row; row = overviewTable.rows[i]; i++) {\n');
  res.write('       let visible=false; // both selections need to turn out true\n');
  res.write('       let strainRow=row.cells[3].innerText; // This may list many strains\n');
//  res.write('       console.log(strainRow + " vs " + selStrains);\n');
  res.write('       for (let strainNo=0, strainSelLength=selStrainsSplitByComma.length; strainNo<strainSelLength; strainNo++) {\n');
  res.write('         let strainQuery = selStrainsSplitByComma[strainNo].trim();\n');
  res.write('         if (strainRow.indexOf(strainQuery) > -1) {\n');
//  res.write('           console.log(strainRow + " vs " + strainQuery);\n');
  res.write('           visible = true;\n');
  res.write('           console.log("Found");\n');
  res.write('           break; // "or" - no need to check other strains\n');
  res.write('         }\n');
  res.write('       }\n');
  res.write('       if (visible) {\n');
  res.write('         let foundMatchingPhen=false;\n');
  res.write('         let phenotypeRow=row.cells[0].getAttribute("data-phenotypes"); // This may list many phenotypes\n');
  res.write('         for (let phenotypeNo=0, phenotypeSelLength=selPhenotypesSplitByComma.length; phenotypeNo<phenotypeSelLength; phenotypeNo++) {\n');
  res.write('           let phenotypeQuery = selPhenotypesSplitByComma[phenotypeNo].trim();\n');
  res.write('           if (phenotypeRow.indexOf(phenotypeQuery) > -1) {\n');
//  res.write('             console.log(phenotypeRow + " vs " + phenotypeQuery);\n');
  res.write('             foundMatchingPhen = true;\n');
//  res.write('             console.log("Found");\n');
  res.write('             break; // "or" - no need to check other phenotypes\n');
  res.write('           }\n');
  res.write('         }\n');
  res.write('         if (foundMatchingPhen) {\n');
  res.write('           visible=true;\n');
  res.write('         } else {\n');
  res.write('           visible=false;\n');
  res.write('         }\n');
  res.write('       }\n');
  res.write('       if (visible) {\n');
  res.write('         row.hidden=false;\n');
//  res.write('         console.log("found");\n');
  res.write('       } else {\n');
  res.write('         row.hidden=true;\n');
//  res.write('         console.log("hiding");\n');
  res.write('       }\n');
  res.write('     }\n');
  res.write('  }\n');
  res.write('}\n');

  res.write('function updatePhenSelectionText() {');
  res.write('  $("p.messages").append("  updatePhenSelectionText - start");\n');
  res.write('  let dest = document.getElementById("selectedPhenotypes");\n');
  res.write('  if (!dest) {\n');
  res.write('    console.log("myPhenotypeClickFunction: Could not find dest.");\n');
  res.write('    return;\n');
  res.write('  }\n');
  res.write('  let div = document.getElementById("myPhenotypeDiv");\n');
  res.write('  let spans = div.getElementsByTagName("span");\n');
  res.write('  dest.value="";\n');
  res.write('  let n=0;\n');
  res.write('  for (let i = 0; i < spans.length; i++) {\n');
  res.write('    let input = spans[i].getElementsByTagName("input")[0];\n');
  res.write('    if (input.checked) {\n');
  res.write('      txtValue = input.name;\n');
  res.write('      if (0<n) dest.value = dest.value + ", ";\n');
  res.write('      dest.value = dest.value + txtValue;\n');
  res.write('      n+=1;\n');
  res.write('    }\n');
  res.write('  }\n');
  res.write('  $("p.messages").append("  updatePhenSelectionText - end");\n');
  res.write('}\n');
  res.write('\n');

  res.write('function myPhenotypeClickFunction(cb) {\n');
  res.write('  $("p.messages").append(" myPhenotypeClickFunction - start");\n');
  res.write('  updatePhenSelectionText();\n');
  res.write('  console.log("myPhenotypeClickFunction("+cb+"): calling mySelectionHandler.");\n');
  res.write('  mySelectionHandler();\n');
  res.write('}\n');
  res.write('\n');

  res.write("function myStrainClickFunction(cb) {\n");
  res.write("  let div = document.getElementById(\"myStrainDiv\");\n");
  res.write("  let dest = document.getElementById(\"selectedStrains\");\n");
  res.write("  if (!dest) {\n");
  res.write("    console.log('myStrainClickFunction: Could not find dest');\n");
  res.write("  }\n");
  res.write("  dest.value=\"\";\n");
  res.write("  let spans = div.getElementsByTagName(\"span\");\n");
  res.write("  let n=0;\n");
  res.write("  for (let i = 0; i < spans.length; i++) {\n");
  res.write("    let input = spans[i].getElementsByTagName(\"input\")[0];\n");
  res.write("    if (input.checked) {\n");
  res.write("      txtValue = input.name;\n");
//res.write("      $(\"p.messages\").append(txtValue);\n");
  res.write("      if (0<n) dest.value = dest.value + ', ';\n");
  res.write("      dest.value = dest.value + txtValue;\n");
  res.write("      n+=1;\n");
  res.write("    }\n");
  res.write("  }\n");
  res.write("  console.log('myStrainClickFunction: calling mySelectionHandler.');\n");
  res.write("  mySelectionHandler();\n");
  res.write("}\n");

  res.write("</script>\n");

  res.write("</head>");
  res.write("<body>");
  res.write("<!-- Read "+compounds.length+" compounds from "+dataDir+" directory. -->\n");
  res.write("<div class=\"imgheader\" style=\"background-image:url(header_worm.jpg);\">");
  res.write("<br><br>");
  res.write("<br><br>");
  res.write("<br><br>");
  res.write("<br><br><br><br><br>");
  res.write("</div>\n");
  res.write("<h1>The Healthy Worm Database</h1>\n");

  res.write("<form method=\"get\" action=\"/form\">\n"); // starting form to help selecting checkboxes in jquery

  res.write("<p id=\"messages\" class=\"messages\"><i>&nbsp;"); //The system may put messages into this field."
  res.write("</i></p>\n");
  res.write("<p class=\"selector\"><button type=\"button\" id=\"toggleIntroduction\">Hide introduction.</button></p>\n");
  res.write("<div id=\"divIntroduction\">\n");
  res.write("<p>");
  res.write("This site displays the results of a literature search for compounds tested for their effects on aging related phenotypes in the nematode <i>Caenorhabditis elegans</i> (<i>C. elegans</i>). It extends the work on the terminology of aging (<a href=\"https://www.aginganddisease.org/EN/10.14336/AD.2018.1030\">Fuellen et al., 2019</a>) and on the search for aging pathways (<a href=\"https://www.aging-us.com/full/12/12534\">M&ouml;ller et al., 2018</a>). For abbreviations see <a href=\"http:abbreviations.html\">abbreviations.html</a>");
  res.write("</p>\n<p>");
  res.write("The collected data is expected to cover all compounds with reports on effects on healthspan that surfaced in the Pubmed search for:<br />\n");
  //QueryPubmedWormCompounds.tif
  res.write("<small><center>\n");
  res.write('(healthspan <font color="red">OR</font> "healthy aging" <font color="red">OR</font> fitness <font color="red">OR</font> "stress resistance" <font color="red">OR</font> homeostasis <font color="red">OR</font> locomotion <font color="red">OR</font> pumping <font color="red">OR</font> "sensory perception" <font color="red">OR</font> memory)<br />\n');
  res.write('<font color="green">AND</font><br />\n');
  res.write('(natural <font color="red">OR</font> compound <font color="red">OR</font> "small molecule" <font color="red"><font color="red">OR</font></font> pharmaceutical <font color="red">OR</font> substance <font color="red">OR</font> extract <font color="red">OR</font> fraction <font color="red">OR</font> fruit <font color="red">OR</font> vegetable <font color="red">OR</font> herb <font color="red">OR</font> chemical <font color="red">OR</font> plant <font color="red">OR</font> "secondary metabolite")<br />\n');
  res.write('<font color="green">AND</font><br />\n');
  res.write('("C. elegans" <font color="red">OR</font> "Caenorhabditis elegans") <font color="green">AND</font> (aging <font color="red">OR</font> ageing) <font color="blue">NOT</font> (Review[ptyp])\n'); 
  res.write("</center></small>\n");
  res.write('</p>\n');

  res.write("<p>\n");
  res.write("We cordially invite you all to inform us about further compounds that should be included and other missing or incorrect details (contact: Nadine.Saul@HU-Berlin.de or Nadine.Saul@gmx.de).");
  res.write(" ");
  res.write("All data and source code is available at https://bitbucket.org/ibima/healthspancompoundswebsite.\n");
  res.write("</p>\n<p>");
  res.write("This work was performed for the EU Horizon 2020 project '<a href=\"https://www.h2020awe.eu/\">Aging with elegans</a>' (633589), by Nadine Saul &lt;Nadine.Saul@HU-Berlin.de&gt; at the Humboldt University of Berlin, Germany, and Steffen M&ouml;ller &lt;steffen.moeller@uni-rostock.de&gt; with Georg Fuellen, <a href=\"https://www.ibima.med.uni-rostock.de\">IBIMA</a>, Rostock University Medical Center, Rostock, Germany.");
  res.write("</p>\n");
  res.write("<p>\n");
  res.write("To cite this work, please consider our recent publication: Saul N, Möller S, Cirulli F, Berry A, Luyten W, Fuellen G. (2021) <i>Health and longevity studies in C. elegans: the 'healthy worm database' reveals strengths, weaknesses and gaps of test compound-based studies.</i> Biogerontology. 22(2):215-236. doi: <a href=\"https://doi.org/10.1007/s10522-021-09913-2\">10.1007/s10522-021-09913-2</a>.\n");
  res.write("</p>\n");

  res.write("<p>\n");
  res.write("<b>References:</b><br />\n");
  res.write("Fuellen G., Jansen L., Cohen A.A., Luyten W., Gogol M., Simm A., Saul N., Cirulli F., Berry A., Antal P., Köhling R., Wouters B., Möller S. (2019) <i>Health and Aging: Unifying Concepts, Scores, Biomarkers and Pathways</i>. Aging and Disease, 10(4), 2. doi: <a href=\"http://www.aginganddisease.org/EN/10.14336/AD.2018.1030\">10.14336/AD.2018.1030</a>.<br />\n");
  res.write("M&ouml;ller S., Saul N., Cohen A., K&ouml;hling R., Sender S., Murua Escobar H., Junghan&szlig; C., Cirulli F., Berry A., Antal P., Adler P., Vilo J., Boiani M., Jansen L., Struckmann S., Barrantes I., Hamed M., Luyten W., Fuellen G. (2020) <i>Healthspan pathway maps in C. elegans and humans highlight transcription, proliferation/biosynthesis and lipids</i> Aging, 12(13):12534-12581. doi: <a href=\"https://www.aging-us.com/article/103514/text\">10.18632/aging.103514</a>.</p>\n");
  res.write("</p>\n");
  res.write("<p>The web interface at the bottom of this page supports an interacive browsing of the data. To <b>download</b> all entries in a flat file that can be imported into R or Excel, also try:\n");
  res.write("<ul>\n");
  res.write("<li><b>Export as Tab-delimited</b>: <a href=\"/flat/all/dot\">all data</a>, only <a href=\"/flat/0/dot\">change</a>, <a href=\"/flat/1/dot\">effect</a>, <a href=\"/flat/2/dot\">P value</a>, <a href=\"/flat/3/dot\">notes</a></li>\n");
  res.write("<li>Overview on: <a href=\"/phenotypegroup/all/html\">phenotype groups</a></li>");
//res.write(", <a href=\"phenotype/all/html\">Phenotypes</a>");
  res.write("\n");
  res.write("</ul>\n");
  res.write("</p>");
  res.write("</div>\n");

/*
  res.write("<p class=\"selector\"><button type=\"button\" id=\"toggleStrainSelection\">Show/hide selection of strains.</button></p>\n");
  res.write("<label class=\"w3-label\" for=\"selectedStrains\">Selected strains: </label>\n");
  res.write("<input class=\"w3-input\" type=\"text\" id=\"selectedStrains\"    onchange='mySelectionHandler(this);' readonly /><p />");
*/

/*
  res.write("<p class=\"selector\"><button type=\"button\" id=\"togglePhenotypeSelection\">Show/hide selection of phenotypes.</button></p>\n");
  res.write("<p>");
  res.write("<label class=\"w3-label\" for=\"selectedPhenotypes\">Selected phenotypes: </label>\n");
  res.write("<input class=\"w3-input\" type=\"text\" id=\"selectedPhenotypes\" onchange='mySelectionHandler(this);' readonly /></p>");
*/

/*
  res.write("<div id=\"divStrainSelection\">\n");
  res.write(modStrains.overview(req,         "myStrainClickFunction(this);")); // gather all strains
  res.write("</div>\n");
*/

/*
  res.write("<div id=\"divPhenotypeSelection\">\n");
  res.write(modPhenotypes.overview(req,      "myPhenotypeClickFunction(this);")); // gather all phenotypes
  res.write(modPhenotypes.overviewGroups(req,"myPhenotypeClickFunction(this);")); // gather all phenotype groups
  res.write("</div>\n");
*/


/*
  res.write("<p>Select phenotypes to show via above checkboxes.</p>\n");
*/

  let t="<div class=\"w3-container\">";
  t += "<p>Search for compounds in the input field: (<i>Please click on compounds listed below or select checkboxes <b>before</b> pressing enter</i>)</p>\n";
  t += "<input class=\"w3-input w3-border w3-padding\" type=\"text\" placeholder=\"Search for compound\" id=\"myCompoundInput\" onkeyup=\"myCompoundFunction()\">\n";
  t += "</div>\n";
  res.write(t);

  res.write("<p class=\"selector\"> ");
  res.write("<button type=\"button\" id=\"toggleStrainDisplayWithCompound\">Show/hide display of strains with compounds</button> - takes some 30 seconds.");
  res.write("</p>\n");
  //let activePhenotypesUnique = modPhenotypes.all.namesUnique();
  //let activePhenotypesUnique = modPhenotypes.phenotypegroups;
  let activePhenotypesUnique = modPhenotypes.phenotypegroups.listPhenotypeNames();
  //res.write("<!-- activePhenotypesUnique "+activePhenotypesUnique + "-->\n");
  console.log("activePhenotypesUnique");
  console.log(activePhenotypesUnique);
  res.write(writeTableOfCompounds(activePhenotypesUnique));

  res.write("</form>\n");
  // jquery activity
  res.write("<script>\n");
  res.write('$("div#divStrainSelection").hide();\n');
  res.write('$("#toggleStrainSelection").text("Show strain selection.");\n');
  res.write('$("div#divPhenotypeSelection").hide();\n');
  res.write('$("#togglePhenotypeSelection").text("Show phenotype selection.");\n');

  res.write('$(document).ready(function() {\n');
  res.write("console.log('I: Setting jquery bindings - start');\n");
  //res.write('$("td.compound").collapse();\n');
//  res.write('$("td.compound").style.height=0px;\n');
  //res.write('$("th.phenotypeheader").collapse();\n');
//  res.write('$("th.phenotypeheader").style.height=0px;\n');
  //res.write('$("th.strains").collapse();\n');
//  res.write('$("th.strains").style.height=0px;\n');
  //res.write('$("td.strains").collapse();\n');
//  res.write('$("td.strains").style.height=0px;\n');

/*
  // binding checkboxes for phenotypes
  for(let j=0,k=activePhenotypesUnique.length;j<k;j++) {
    let phen=activePhenotypesUnique[j];
    let phenNoBlanks=phen.replace(/[ ;,'()]/g,"");
    res.write('$("input:checkbox:'+phenNoBlanks+'").change(function() {\n"'
            + '   $("input:checkbox:#checkbox2").prop("checked", this.checked);'
            + '});');
  }
*/

  res.write(
      ' $("input:checkbox.phenotype").bind("change",function(){\n'
     +'    let phen=$(this).prop("name");\n'
     +'    $("p.messages").append("<br>checkbox "+phen+" changed\\n");\n'
     +'    console.log("checkbox "+phen+" changed");\n'
     +'    $("input:checkbox.phenotypealone[name="+phen+"]").prop("checked",this.checked);\n'
     +'    $("input:checkbox.phenotypeingroup[name="+phen+"]").prop("checked",this.checked);\n'
 //    +'    if ($(this).is(":checked")) {\n'
    // +'      $("p.messages").append("<br>show "+phen+"\\n");\n'
   //  +'      console.log("show "+phen);\n'
     //+'    } else {\n'
    // +'      console.log("Hide "+phen);\n'
 //    +'      $("th."+phen).collapse();\n'
 //    +'      $("td."+phen).style.height=0px;\n'
 //    +'      $("th."+phen).collapse();\n'
//     +'      $("td.compoundheader[data-phenotypes*=phen]").hide();\n'
 //    +'      $("td.compound[data-phenotypes="+phen+"]").hide();\n'
  //   +'      $("th.phenotypeheader[data-phenotypes="+phen+"]").hide();\n'
 //    +'      $("td."+id).style.height=0px;\n'
    // +'    }\n'
 //    +'  myPhenotypeClickFunction("");\n'
     +'    updatePhenSelectionText();\n'
     +'    mySelectionHandler();\n'
     +'  });\n'
  );
  res.write("console.log('I: Setting jquery bindings - completed');\n");
  res.write('}); // end of ready()\n');

function toggle(objectId,showText,hideText) {
  r = "";
  r += '$("#toggle'+objectId+'").bind("click",function(){\n';
  r += '  $("p.messages").append(" Clicked p.toggle'+objectId+'");\n';
  r += '  if($("div#div'+objectId+'").is(":visible")) {\n';
//  r += '    $("p.messages").append(" visible - hiding");';
  r += '    $("div#div'+objectId+'").hide();\n';
  r += '    $("#toggle'+objectId+'").text("'+showText+'");\n';
  r += '  } else {\n';
//  r += '    $("p.messages").append(" hidden - showing");';
  r += '    $("div#div'+objectId+'").show();\n';
  r += '    $("#toggle'+objectId+'").text("'+hideText+'");\n';
  r += '  }';
  r += '});\n';
  return(r);
}
  res.write(toggle("Introduction","Show introduction","Hide introduction"));
  res.write(toggle("StrainSelection","Show strain selection","Hide strain selection"));
  res.write(toggle("PhenotypeSelection","Show phenotype selection","Hide phenotype selection"));

/*
res.write('function toggleStrainDisplayWithCompound() {\n');
res.write('  if($("th.strains").is(":visible")) {\n');
res.write('    $("th.strains").hide();\n');
res.write('    $("tr.compound").each(function(){\n');
res.write('      if ($(this).is(":visible")){\n');
res.write('          $(this).find("td.strains").hide();\n');
res.write('      }\n');
res.write('    }); // each\n');
res.write('  } else {\n');
res.write('    $("th.strains").show();\n');
res.write('    $("tr.compound").each(function(){\n');
res.write('      if ($(this).is(":visible")){\n');
res.write('          $(this).find("td.strains").show();\n');
res.write('      }\n');
res.write('    }); // echo\n');
res.write('  }\n');
res.write('}\n');
  res.write('$("p#toggleStrainDisplayWithCompound").bind("click","toggleStrainDisplayWithCompound");\n');
*/
  res.write('function implementationStrainDisplayWithCompound(){\n');
  res.write('  $("p.messages").append(" Clicked p#toggleStrainDisplayWithCompound.");\n');
  res.write('  if($("th.strains").is(":visible")) {\n');
  res.write('    $("th.strains").hide();\n');
  res.write('    $("tr.compound").each(function(){\n');
  res.write('      if ($(this).is(":visible")){\n');
  res.write('          $(this).find("td.strains").hide();\n');
  res.write('      }\n');
  res.write('    });');
  res.write('  } else {\n');
  res.write('    $("th.strains").show();\n');
  res.write('    $("tr.compound").each(function(){\n');
  res.write('      if ($(this).is(":visible")){\n');
  res.write('          $(this).find("td.strains").show();\n');
  res.write('      }\n');
  res.write('    });');
  res.write('  }');
  res.write('}\n');
  res.write('$("p#toggleStrainDisplayWithCompound").bind("click",implementationStrainDisplayWithCompound);\n');
  res.write('$("p#hideStrainsColumn").bind("click",implementationStrainDisplayWithCompound);\n');

  res.write("</script>\n");
  res.write("</body>\n");
  res.write("</html>\n");
  res.end();
});

/**
 * Function to write the table of compounds
 *
 * @returns: HTML string with table
 */
function writeTableOfCompounds(activePhenotypesUnique) {
  var res="<div class=\"w3-container\">\n";
  res +="<table id=\"compoundsOverview\" cellspacing=\"0\" cellpadding=\"1\">\n";
  res += "<thead>\n";
  res += "<tr class=\"header\">\n";
  res += "<th class=\"header compound id\">ID</th>";
  res += "<th class=\"compound name\">Name (PubChem)</th>";
  res += "<th class=\"compound exp\">#Cond / #Phen</th>";
  res += "<th class=\"compound strains\">Strains<p id=\"hideStrainsColumn\"><i>(hide)</i></p></th>\n";
  for(let i=0,l=activePhenotypesUnique.length;i<l;i++) {
    let h=activePhenotypesUnique[i];
    let hNoBlanks=h.replace(/[ ;,'()]/g,"");
    let phenGroupname=modPhenotypes.phenotypeName2phenotypeGroupname(h);
    res += "<th class=\"phenotypeheader\" data-phenid=\""+i+"\" data-phenotypes=\""+hNoBlanks+"\"";
    res += " bgcolor=\""+modDecoration.phenotypeGroupname2colour(phenGroupname)+"\"";
    res += ">";
    res += "<div><span>"+modDecoration.simplifyGroupname(h);
    //res += ' <i>(' + phenGroupname + ")</i>"
    res += "</span></div>";
    res += "</th>\n";
  }
  res += "</tr>\n";
  res += "</thead>\n";
  res += "<tbody>\n";

  console.log("I: compounds.length="+compounds.length);
  for(let i=0,l=compounds.length,lineNoShown=0;i<l;i++) {
    lineNoShown++;
    let CompoundNameSave=compounds[i].name.replace(/[<>&"]/g,"");
    let compoundNameNoBlanks=CompoundNameSave.replace(/[ ;()']/g,"");
    let textStart="<tr";
    textStart += " class=\"compound compoundheader compound"+compounds[i].id;
    if (0==lineNoShown%2) {
      textStart += " compoundeven";
    } else {
      textStart += " compoundodd";
    }
    textStart += "\""; // end of class assignments
    textStart += " id=\""+compoundNameNoBlanks+"\"";
    textStart += ">\n";

    let numFound=0;
    let textCompound="";
    let textPhenotypes=[];
    let textPhenotypesId=[];
    for(let phenid=0,k=activePhenotypesUnique.length;phenid<k;phenid++) {
      let phen=activePhenotypesUnique[phenid];
      textPhenotypesId.push(phenid);
      textCompound += "<td data-phenid=\""+phenid+"\" data-field=\""+i+"_"+phenid+"\" data-compoundid=\""+i+"\" class=\"compound";
      if (typeof phen == 'undefined') {
	textCompound += "\">"; // ending class specification and closing <td
	textCompound += "undefined";
        textCompound += "</td>\n";
      } else {
        let pindexof=compounds[i].phenotypes.indexOf(activePhenotypesUnique[phenid]);
	let phenNoBlanks=phen.replace(/[ ;,'()]/g,"");


	if (pindexof>-1) {
          let h=activePhenotypesUnique[phenid];
          let phenGroupname=modPhenotypes.phenotypeName2phenotypeGroupname(h);
          textCompound += " phenWithContent\""; // ending class specification
          textCompound += " data-phenotypes=\""+phenNoBlanks+"\"";
          textCompound += " bgcolor=\""+modDecoration.phenotypeGroupname2colour(phenGroupname)+"\"";
          textCompound += ">"; // ending <td
          //textCompound += pindexof;
          textPhenotypes.push(phenNoBlanks);
	} else {
          textCompound += " phenWithNoContent\""; // ending class specification and ending <td
          textCompound += " data-phenotypes=\""+phenNoBlanks+"\"";
          //textCompound += " bgcolor=\"darkgray\"";
          textCompound += " bgcolor=\"#a9a9a9\"";
          textCompound += ">"; // ending <td
	}
        textCompound += "</td>\n";
      }
      numFound++;
    }

    textStart += "<td nowrap class=\"compound compoundheader"
	//"\" data-phenid=["+textPhenotypesId.join(",")+"]"
		+"\" data-phenotypes=\""+textPhenotypes.join(" ")
		+"\" data-compoundid=\""+i+"\">";
    textStart += "<input type=\"checkbox\" name=\"id[]\" value=\""+compounds[i].id+"\" />";
    textStart += "<a href=\"/compound/"+compounds[i].id+"/html\">"+i+"</a>";
//    textStart += "</input>";
    textStart += "</td>\n";
    textStart += "<td class=\"compound compoundheader pubchem"
	//"\" data-phenid=["+textPhenotypesId.join(",")+"]"
	+"\" data-phenotypes=\""+textPhenotypes.join(" ")
		+"\" data-compoundid=\""+i+"\">";
    //textStart += CompoundNameSave;
    textStart += "<a href=\"/compound/"+compounds[i].id+"/html\">"+CompoundNameSave+"</a>";
    if ("" != compounds[i].pubchem) {
      textStart += " (<a href=\"https://pubchem.ncbi.nlm.nih.gov/compound/"+compounds[i].pubchem+"\">"+compounds[i].pubchem+"</a>)";
    }
    textStart += "</td>\n";

    textStart += "<td class=\"compound exp"
	// +"\" data-phenid=\""+textPhenotypesId.join(" ")
		+"\" data-phenotypes=\""+textPhenotypes.join(" ")
		+"\" data-compoundid=\""+i+"\">"
		+( compounds[i].nlines-3 ) 
	+ " / "
	+ textPhenotypes.length
		+"</td>\n";
    textStart += "<td class=\"compound compoundheader strains"
	// +"\" data-phenid=\""+textPhenotypesId.join(" ")
		+"\" data-phenotypes=\""+textPhenotypes.join(" ")
		+"\" data-compoundid=\""+i+"\">"
		+compounds[i].strainNamesUnique().join(", ")
		+"</td>\n";
    if (numFound>-1) {
      res += textStart + textCompound +"</tr>\n";
    } else {
      console.log("W: Skipped entry "+i);
    }
  }

  res += "</tbody>\n";
  res += "</table>\n";
  res += "<p>";
  res += "<input type=\"submit\" value=\"Submit\" id=\"submit\" name=\"format\"> See details of selected compounds</input>\n";
//  res += "<input type=submit value=text id=submit name=format>See Details at Text/TSV</input>\n";
  res += "</p>";
  res += "</div>\n";
  res += "<hr />End of Table<hr />\n";

  return(res);
}

app.listen(port, "0.0.0.0", () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
