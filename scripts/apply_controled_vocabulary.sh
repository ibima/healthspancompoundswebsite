#!/usr/bin/perl -w

use strict;
use File::Slurp;

if ("-h" eq $ARGV[1] || "--help" eq $ARGV[1]) {
	print <<EOHELP;
USAGE: cat vocabulary*txt | apply_controled_vocabulary.pl data/*

This script performs a series of substitutions throughout the data retrieved from
google to help the system in adjusting to a strict set of vocabulary. This anticipated
for phenotypes, phenotype groups and strains alike.
EOHELP
	exit;
}

my @files = glob("*");
print @files;

# small routine to remove blanks/double quotes from strain names
sub makeNeat {
	my $ref=shift;
	$ref =~ s/["]//g;
	$ref =~ s/ *$//;
	$ref =~ s/^ *//;
	return($ref);
}

my %substitutions;


my $lineno=0;
while(<>) {
	$lineno++;
	# skip header
	#next if 1>=$lineno;

	my $l=shift;
	chomp;
	next if /hier stimmen Stamm und Genotyp nicht/;
	next if /^#/;
	next if /^[ \t]*$/;
	# neat excel export
	$_ =~ s/[\t\n\r ]+$//;

	my @addTheseProperOnes=(); # these are the values we aim to match and substitute
	my ($ref,@synonyms) = split(/\t/);
	$ref = makeNeat($ref);
	if ("" eq $ref) {
		print STDERR "E: '' eq \$ref in line '$l' - fix files and script\n";
		next;
	}
	if (exists($substitutions{$ref})) {
		@addTheseProperOnes=@{$substitutions{$ref}};
	}
	foreach my $s (@synonyms) {
		$s = makeNeat($s);
		if ($s =~ /^$/) {
			print STDERR "W: found empty substitute in line '$l'\n";
			next;
		}
		if ($s eq $ref) {
			print STDERR "W: substitute '$s' points to itself '$ref' - skipped\n";
			next;
                } else {
			push @addTheseProperOnes,$s;
			print STDERR "ref: addTheseProperOnes: ";
			print join(", ",@addTheseProperOnes);
			print "\n";
                }
		print STDERR "** Found food rule on HT115: ($s -> $ref)\n" if $s =~ /HT115/;
	}


	$substitutions{$ref}=\@addTheseProperOnes;

}

foreach my $k (sort keys %substitutions) {
	print "$k <- ";
	print join(", ",@{$substitutions{$k}});
	print "\n";
}

#exit();

foreach my $f (@files) {
	print STDERR "I: Working on '$f'\n";
	my $ftext = read_file($f);
	my $modified=0;	
	if ($ftext =~ / +$/) {
		$modified=1;
		$ftext =~ s/ +$//g
	}
	if ($ftext =~ /^ +/) {
		$modified=1;
		$ftext =~ s/^ +//g
	}
	if ($ftext =~ / +\t/) {
		$modified=1;
		$ftext =~ s/ +\t/\t/g
	}
	if ($ftext =~ /\t +/) {
		$modified=1;
		$ftext =~ s/\t +/\t/g
	}
	foreach my $ref (sort keys %substitutions) {
		my @synonyms = @{$substitutions{$ref}};
		foreach my $s (@synonyms) {
			my $debug=1;
			$debug=1 if $s =~ m/HT115/;
			$debug=1 if $s =~ m/^18.*gl/;
			#print STDERR " **** DEBUG ON *** for HT115\n";
			$s = makeNeat($s);
			if ($ftext =~ m/\t\Q$s\E($|\t)/) {
				$modified = 1;
				print STDERR "***** on $f: $s -> $ref\n" if $debug;
				$ftext =~ s/\t *\Q$s\E *\t/\t$ref\t/g;
				$ftext =~ s/\t *\Q$s\E *$/\t$ref/g;
			}
		}
	}
	if ($modified) {
		#my $destname="$f.update";
		my $destname="$f";
		open(FH,">$destname") or die "E: Could not open file $destname for writing.\n";
		print FH $ftext;
		close(FH);
	}
}


