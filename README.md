# README #

### What is this repository for? ###

This repository orchestrates the development and distribution of a
web site to present literature search data on natural compounds that
affect the HealthSpan.

### How do I get set up? ###

It is a node.js project, i.e. server-side JavaScript, implementated with
help of the Express module. Data is provided by the 
https://bitbucket.org/ibima/healthspancompoundsliterature
repository.

To start the application, run
  node app.js
on the command line from this working directory.

### Whom do I talk to? ###

For contributions or questions please contact
 Steffen Moeller <firstname.lastname@uni-rostock.de>.
 Nadine Saul <firstname.lastname@HU-Berlin.de>
