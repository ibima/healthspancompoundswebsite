const modCompound=require('./modCompound');
class Strain {
}

Strain.names = new Array();
Strain.registry={
};


exports.strainRegistryAddCompound = function (comp) {

  if (typeof comp == "undefined") {
    console.log("E: modStrains::strainRegistryAddCompound: comp is undefined");
    return;
  }

  compId = comp.id;
  if (typeof compId == "undefined") {
    console.log("E: modStrain::strainRegistryAddCompound: id of comp passed is undefined. comp: " + comp);
    return;
  }

  console.log("I modStrains::strainRegistryAddCompound: invoking comp.strainNamesUnique()");
  let strainNamesArray=comp.strainNamesUnique();
  if ("undefined" == typeof strainNamesArray) {
    console.log("E modStrains::strainRegistryAddCompound: strainNamesArray returned from comp.strainNamesUnique() was undefined.");
    return;
  }

  for(let i=0,l=strainNamesArray.length;i<l;i++) {
    let strainName=strainNamesArray[i];
    if (typeof  strainName == "undefined") {
      console.log("I strainRegistryAddCompound: undefined ");
    } else {
      let strainNameIndex=Strain.names.indexOf(strainName);
      if (-1 == strainNameIndex) {
        console.log("I: new strain: "+strainName);
        Strain.registry[strainName]=new Array()
        Strain.names.push(strainName);
      }
      Strain.registry[strainName].push(comp.id);
      console.log("I:   now shows:"+Strain.registry[strainName]);
    }
  }

}

exports.strainRegistryName2Compounds = function(strainName) {
  if (typeof strainName == "undefined") {
    console.log("E: modStrains::strainRegistryName2Compounds: strainName undefined");
    return(["undefined strainName passed"]); 
  }
  let strainNameIndex=Strain.names.indexOf(strainName);
  if (-1 == strainNameIndex) {
    console.log("E: modStrains::strainRegistryName2Compounds: Was asked about '"+strainName+"' which was not found. Known strain names are "+ Strain.names);
    return(["strainName '"+strainName+"' not known, only knowing"+Strain.names]); 
    //return(undefined); 
  }
  let r = Strain.registry[strainName];
  if (typeof r == "undefined") {
    console.log("E: modStrains::strainRegistryName2Compounds: Failed lookup of '"+strainName+"' even though strain is known, this is what I have in registry: "+Strain.registry);
    return(["strainName '"+strainName+"' not found, only registered: "+Strain.registry]); 
  }
  //console.log("E: strainRegistryName2Compounds: results should be legit: "+r);
  return(r);
}

exports.strainRegistryGetNames=function() {
  return(Strain.names);
}

// return overview as HTML string, do not write to res
exports.overview = function (req,onClickFun) {
  let ns=Strain.names;
  let t="<div class=\"w3-container\">";
  t +=("<p>Observed "+ns.length+" strains.</p>\n");
  t += "<p>Search for Strains in the input field:\n";
  t += "<input class=\"w3-input w3-border w3-padding\" type=\"text\" placeholder=\"Search for Strains\" id=\"myStrainInput\" onkeyup=\"myStrainFunction()\">\n";
  t += "<div class=\"outer\" id=\"myStrainDiv\">\n";
  ns.sort();
  for(let i=0,l=ns.length;i<l;i++) {
    let n=ns[i];
    let nNoBlanks=n.replace(RegExp("[\"' ]","g")," ");
    nNoBlanks=nNoBlanks.replace(RegExp("^ *","")," ");
    t += "<span";
    if (1==i%2) {
      t += " class=\"straineven\"";
    } else {
      t += " class=\"strainodd\"";
    }
    t += "><input type=\"checkbox\" class=\"strain\" name='"+nNoBlanks+"' onclick='"+onClickFun+"'";
    t += "/>"+n+"</span>\n";
  }
  t += "</div>\n";
  t += "</div> <!-- container -->\n";
  t += "\n";
  t += " <script>\n";
  t += "function myStrainFunction() {\n";
  t += "  let input = document.getElementById(\"myStrainInput\");\n";
  t += "  let filter = input.value.toUpperCase();\n";
  t += "  let div = document.getElementById(\"myStrainDiv\");\n";
  t += "  let spans = div.getElementsByTagName(\"span\");\n";
  t += "  for (let i = 0; i < spans.length; i++) {\n";
  t += "    input = spans[i].getElementsByTagName(\"input\")[0];\n";
  t += "    if (input) {\n"
  t += "      let txtValue = input.name;\n";
  t += "      if (txtValue.toUpperCase().indexOf(filter) > -1) {\n";
  t += "        spans[i].style.display = \"\"; }\n";
  t += "      else {\n";
  t += "        spans[i].style.display = \"none\";\n";
  t += "      }\n";
  t += "    }\n";
  t += "  }\n";
  t += "}\n";
  t += "</script>\n";
  return(t);
}
