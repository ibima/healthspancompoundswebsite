#!/bin/bash

if ps ux|grep -v grep|grep -q node; then
    killall -u $(whoami) node
fi
(node app.js | tee app.log)&
date
