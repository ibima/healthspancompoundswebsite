#!/usr/bin/python3
from __future__ import print_function

from pprint import pprint
from googleapiclient import discovery
from time import sleep
import re

# https://developers.google.com/sheets/quickstart/python#step_3_set_up_the_sample
#
# Authorize using one of the following scopes:
#     'https://www.googleapis.com/auth/drive'
#     'https://www.googleapis.com/auth/drive.file'
#     'https://www.googleapis.com/auth/drive.readonly'
#     'https://www.googleapis.com/auth/spreadsheets'
#     'https://www.googleapis.com/auth/spreadsheets.readonly'
credentials = None

service = discovery.build('sheets', 'v4', credentials=credentials)

# The spreadsheet to request.
# Steffen's stupid test with two sheets
#spreadsheetId = '1aB5vo1npcfSGsUdo8PdGx_EhYjnKk66Dqw388_979qw'  # TODO: Update placeholder value.
# Nadine's database

spreadsheetIDs = {
  "A":  "1Q7hUv1lyBg0QwjrptM3pAvxcPpUvkNLWAJsLwgsIA7I",
  "B":  "1PBET3n4VbRbBkDDHRMp5_5R5uncY3XdX_XsASK2rHLU",
  "C":  "1ZLzY2d2UiA3kJYkEu0qhQtlUA8SkAzPTWfZzfk6f4wQ",
  "D":  "1VZS0dFV4_-Gwm05bcYcebP-ljgbpfXJN-ToxQJ8TxWk",
  "E":  "19Jid0F4dihzE_8bdvig6WxeLd4L5MfEnvRgrN24_x9o",
  "F1": "1-Gws3brDEbHon0U4wNsmov9qlRWhxStJ5whCOT2v3QM",
  "F2": "1ar9EgGnyVrMgO7eFXkS8nqf6Mf0pxqP4kgSlE-t4CYY",
  "G":  "1P3x466ermnjhHMJtHR7X6302HHKu6D2qjqKdGnsw9xs"
}

path="googledata"
m = open(path+"_mapping.tsv","w")

def title2filename(t):
   #return(t.encode('utf-8').replace("/","_").replace("'","").replace("\\","").replace(" ","_")+".tsv")
   a=str(t)
   b=a.replace('/','_')
   c=b.replace('\'',"").replace('\\',"").replace(" ","_")
   return(c+".tsv")

i=0
for filename in spreadsheetIDs:

   spreadsheetId = spreadsheetIDs[filename]

   request = None
   request = service.spreadsheets().get(spreadsheetId=spreadsheetId, includeGridData=False)
   response = None
   response = request.execute()

   rangesArray=[]
   j=0
   for sheet in response["sheets"]:
      sheetId=sheet["properties"]["sheetId"]
      sheetTitle=sheet["properties"]["title"]
      #print(sheetId, " : ", sheetTitle)
      range=sheetTitle+"!"+"A1:NP200"  # attention - having this too slim/short will miss the rightmost/lower parts
      rangesArray.append(range)
      m.write(title2filename(sheetTitle))
      m.write("\t")
      m.write(str(sheetTitle.encode('utf-8')))
      m.write("\t"+str(sheetId))
      m.write("\t"+spreadsheetId)
      m.write("\t"+"https://docs.google.com/spreadsheets/d/"+spreadsheetId+"/edit#gid="+str(sheetId))
      m.write("\n")

   request2 = None
   request2 = service.spreadsheets().values().batchGet(spreadsheetId=spreadsheetId, ranges=rangesArray)
   response2 = None
   response2 = request2.execute()

   #pprint(response2)

   print("I: sleeping 5 seconds - avoiding trouble with Google")
   sleep(5)

 
   j=0
   for r in response2["valueRanges"]:
      n = re.findall("^[^!]+",r["range"])[0]
      v = "N/A"
      f = open(path+"/"+title2filename(n),"w")
      k=0
      if r.get("values") is not None:
         for row in r["values"]:
            l=0
	    #row max contain newlines
            #f.write(u'\t'.join(row).encode('utf-8').strip())
            for v in row:
               if (l>0):
                  f.write("\t")
               #f.write(v.replace(u'\n',"").replace(u'\r',""))
               vs = v.encode('utf-8').splitlines()
               #vs = v.splitlines()
               for vv in vs:
                  vvv=vv.decode('utf-8')
                  s=vvv.replace('\t',"")
                  ss=s.strip()
                  f.write(ss)
               l=l+1
            f.write('\n')
            k=k+1
      print(i,str(filename),j,n,k)
      i = i+1
      j = j+1
      f.close()

m.close()

