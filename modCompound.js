/**
 *
 *
 * This file implementes the interpretation of a single sheet of the database which represents a single compound for many experiments.
 * The last column(s) accomodate(s) the reference to the literature from which all other info was derived. Line 3 knows about the nature of that reference - Reference to literaure, transcriptome, proteome, or metabolome.
 * Update: An ultimate column "blinded" was added.
 * 
 *    Compound -> [Row]->...
 */
const modDecoration = require('./modDecoration');
const fs = require('fs');
const path = require('path');
const os = require('os');

const strainFieldPos=5;


class Row {
  constructor(s,no) {
    this.number = no
    this.reference = "";
    this.referencePMID = undefined;
    this.referenceDOI = undefined;
    this.blinded = "";
    this.trancriptome = "";
    this.proteome = "";
    this.metabolome = "";
    this.target = "";
    this.fields = undefined;

    if (typeof s == "undefined") {
      this.fields = new Array();
    } else {
      this.fields = s.split("\t");
    }
  }
  empty(){
    if (typeof this.fields == "unknown") return true;
    for(let f in this.fields) {
      if (typeof f != "undefined" && f.trim() != "") {
        return(false);
      }
    }
    return(true);
  }
}
// Number of fields that all rows have right at the beginning
Row.NumDescFields=7; // Concentration (pos 0), Exposure Time, Food, Growth medium, Special addition, Strain, Degrees (pos 6)

class Compound {

  constructor() {
    this.id=Compound.number;
    this.simplified=undefined;
    this.url=undefined; // pointer to Google URL where this compound was described
    this.name=undefined
    this.directory=undefined
    this.filename=undefined
    this.pubchem=undefined
    this.header1=undefined
    this.header2=undefined
    this.header3=undefined
    this.phenotypes=new Array();      // names of phenotypes in order of their appearance in this file
    this.phenotypesGroup=new Array(); // names of phenotypegroups in order of their appearance in this file
    this.phenotypesIndex=new Array(); // positions within row.fields at which phenotype is first found - this and the next three positions are constituting it
    this.lines=undefined;
    this.rows=new Array();
    this.nlines=0;
    this.strainsUnique = new Array();
    this.content=undefined;
    this.referenceColNo=undefined; // starting with 0
    this.transcriptomeColNo=undefined; // starting with 0
    this.proteomeColNo=undefined; // starting with 0
    this.metabolomeColNo=undefined; // starting with 0
  };

  nrow() {
    if(undefined(this.rows)) return 0;
    return(this.rows.length);
  };

  ncol() {
    if(undefined(this.rows)) return 0;
    return(Math.max(Math.max(
                              this.rows[0].fields.length,
                              this.rows[1].fields.length),
                    this.rows[2].fields.length)
                   );
  };

  dataPresenceTest(colNumber) {
    for(let i=3,l=this.rows.length;i<l;i++) {
      if (typeof this.rows[i].fields[colNumber] !== "undefined" && "" !== this.rows[i].fields[colNumber]) {
        //console.log("I ("+this.id+"): dataPresenceTest: Found phenotype for "+this.name+" at col "+colNumber+" in line "+i);
        return(true);
      }
    }
    //console.log("I ("+this.id+"): dataPresenceTest: No phenotype for "+this.name+" at col "+colNumber);
    return(false);
  };


  readFromFileSync(dir,filename) {
    this.directory = dir;
    this.filename = filename;
    this.simplified=false;
    //this.name = filename.slice(21);
    let fullPath=dir+"/"+filename;
    let f = fs.openSync(fullPath,"r");
    this.content = fs.readFileSync(f,"utf8"); //.split("\n");
    fs.closeSync(f);
    //console.log(this.content);
    //this.lines = this.content.split(os.EOL);
    this.lines = this.content.split("\n");
    if (this.lines[0].endsWith("\r")) {
      this.lines = this.content.split("\r\n");
    }
    this.nlines = this.lines.length;
    //console.log(this.lines)

    // reading rows
    let encounteredEmptyRow=false;
    for(let i=0,l=this.nlines;i<l;i++) {
      let r = new Row(this.lines[i],i);
      if (r.empty()) {
        if (encounteredEmptyRow) {
          console.log("W compound("+this.id+").readFromFileSync("+filename+"): encountered 2nd emptyRow (line "+i+")");
        }
      } else {
        this.rows.push(r);
      }
    }
    console.log("I ("+this.id+"): number of rows: "+this.rows.length);

    this.header1=this.rows[0];
    console.log("I ("+this.id+"): this.header1: "+this.header1.fields)
    console.log("I ("+this.id+"): this.header1.fields[0]: "+this.header1.fields[0])
    console.log("I ("+this.id+"): this.header1.fields[1]: "+this.header1.fields[1])
    console.log("I ("+this.id+"): this.header1.fields[2]: "+this.header1.fields[2])
    //this.name = this.header1.fields[2];//with manual save
    this.name = this.header1.fields[1];// with google auto retrieval
    console.log("I ("+this.id+"): this.name: "+this.name)
    if ("undefined" == typeof this.name || "" == this.name) {
      //this.name = path.parse(filename).base.slice(21);
      this.name = path.parse(filename).base;
      console.log("E ("+this.id+"): undefined name for "+this.filename+"\n");
    }
    this.header2=this.rows[1];
    this.header3=this.rows[2];
    console.log("I ("+this.id+"): this.header2: "+this.header2.fields)
    console.log("I ("+this.id+"): this.header3: "+this.header3.fields)

    //console.log(this);
    // push list of columns with data
    //console.log("I ("+this.id+"): Retrieving used phenotypes for '"+this.name+"' with header.length="+this.header2.fields.length+"\n");
    let currentPhenGroup="";
    for(let i=Row.NumDescFields, specialsFound=false, l1=this.header3.fields.length, l2=this.header3.fields.length, l3=this.header3.fields.length,lmax=Math.max(l1,Math.max(l2,l3));
        i<lmax;
        i++) {
      let f = this.header2.fields[i]; // phenotype
      let g = this.header1.fields[i]  // group
      if (!specialsFound) {
        let s = this.header3.fields[i].trim(); // special
        if ("Reference"==s || "Supposed interactions with genes, proteins, pathways"==s) {
          specialsFound=true;
        }
      }
      if (typeof(f) == "undefined" || l2 < i) f="";
      else f=f.trim();
      if (typeof(g) == "undefined" || l1 < i) g="";
      else g=g.trim();

      if ("" == g) {
        g=currentPhenGroup;
        if (i >= l1) {
	  this.header1.fields.push(g);
        } else {
	  this.header1.fields[i]=g;
        }
      } else {
        currentPhenGroup=g;
      }
      //console.log(" ("+this.id+")f="+f+"\n");
      if("" !== f && " " !== f && "  " !== f) {
        if (this.dataPresenceTest(i)) {
          this.phenotypes.push(this.header2.fields[i]);
          this.phenotypesIndex.push(i);
          this.phenotypesGroup.push(g);
        }
      }
    }

    this.name = this.header1.fields[1].trim();
    if (typeof this.header2.fields[1] == "undefined") {
      this.pubchem = "";
    } else {
      this.pubchem = this.header2.fields[1].trim();
    }
  };

  simplify(compound) {
    let r = new Compound();
    r.id = this.id;
    r.simplified=true;
    r.name = this.name;
    r.filename = this.filename;
    r.directory = this.directory;
    r.pubchem = this.pubchem;
    r.nlines = this.nlines;
    r.url = this.url;
    r.rows = new Array();

    let prefReference="";
    // copying of values needs to follow
    // iterating over rows of complete entry
    for(let n=0,nl=this.nlines;n<nl;n++) {

      let tl=this.rows[n];
      let rl=new Row(undefined,n);

      // some basic checks on sanity of this row - some empty rows may have been copied from Google ables
      for(let i=0; i<Row.NumDescFields; i++) {
        if (typeof tl.fields[i] == "undefined") {
          tl.fields[i] = "";
        }
      }

      if (n>=3 && (typeof tl.fields[1]=="undefined" || tl.fields[1]=="")
               && (typeof tl.fields[2]=="undefined" || tl.fields[2]=="")
               && (typeof tl.fields[3]=="undefined" || tl.fields[3]=="")
               && (typeof tl.fields[4]=="undefined" || tl.fields[4]=="")
               && (typeof tl.fields[5]=="undefined" || tl.fields[5]=="")) {
        console.log("W simplify: Compound "+this.id+" ("+this.name+") had empty metadata at row "+n+" of "+this.nlines+" - skipped");
        r.nlines--;
        continue;
      }

      // copy row metadata
      for(let i=0,il=Row.NumDescFields; i<il; i++) {
        let v=tl.fields[i];
        //console.log(v);
        if (typeof v == "undefined") {
          console.log("W simplify: Compound "+this.id+" ("+this.name+") had unknown value at row "+n+" field "+i+" between fields 0 and "+Row.NumDescFields);
          rl.fields.push("");
        } else {
          rl.fields.push(v.trim());
        }
      }

      if (n>=3) {
        // every line may have a different length because of Google's optimisations
        // looking from right to left
        for(let fieldno=tl.fields.length-1;fieldno>tl.fields.length-1-5; fieldno--){
          let origHeaderField=this.rows[2].fields[fieldno];
          //console.log("I simplify ("+this.id+") Reference orig header(2/"+this.nlines+"), fieldno: "+fieldno+": "+origHeaderField);
          let v=tl.fields[fieldno];
          if(origHeaderField=="Reference") {
             if (typeof v != "undefined") {
                if ("" == v) {
                   rl.reference=prefReference;
                   console.log("I simplify ("+this.id+") Reference assigned from previous line's content (1)")
                } else {
                   rl.reference=v.trim();
                   prefReference=rl.reference;
                   console.log("I simplify ("+this.id+") Reference stored in prefReference")
                }
             } else {
                rl.reference=prefReference;
                console.log("I simplify ("+this.id+") Reference assigned from previous line's content (2)")
             }
             this.referenceColNo=fieldno;
             console.log("I simplify ("+this.id+") Reference at rowno: "+n+", nlines: "+ this.nlines+", fieldno: "+fieldno+" -> "+rl.reference);
          } else if(origHeaderField=="Transcriptomic data") {
             if (typeof v != "undefined") rl.transcriptome=v.trim();
             this.transcriptomeColNo=fieldno;
          } else if(origHeaderField=="Proteomic data") {
             if (typeof v != "undefined") rl.proteome=v.trim();
             this.protemeColNo=fieldno;
          } else if(origHeaderField=="Metabolomic data") {
             if (typeof v != "undefined") rl.metabolome=v.trim();
             this.metabolomeColNo=fieldno;
          } else if(origHeaderField=="Supposed interactions with genes, proteins, pathways") {
             if (typeof v != "undefined") rl.target=v.trim();
             this.targetColNo=fieldno;
          } else if(origHeaderField=="blinded") {
             if (typeof v != "undefined") rl.blinded=v.trim();
             this.targetColNo=fieldno;
          }
        }
      }

      // if lines are too short then the reference field is not reached
      if (typeof rl.reference == "undefined") {
         rl.reference=prefReference;
      } else if ("" == rl.reference) {
         rl.reference=prefReference;
         console.log("I simplify ("+this.id+") Reference assigned from previous line's content (3)")
      }

      if (typeof rl.reference != "") {
         const patt = /(DOI|doi): ?([0-9.]+[0-9]\/[0-9a-z/.]+[0-9a-z])/;
         var result = patt.exec(rl.reference);
         if (result) {
            rl.referenceDOI=result[2];
            console.log("I simplify ("+this.id+") Reference assigned DOI '"+rl.referenceDOI+"'")
         } else {
            console.log("I simplify ("+this.id+") Reference misses DOI")
         }
         console.log("I simplify ("+this.id+") Reference assigned from previous line's content (3)")
      }

      r.rows.push(rl);
    }

    // copy data assigned to phenotyes
    let phenotypePos=Row.NumDescFields;
    for(let i=0,l=this.phenotypes.length;i<l;i++) {
      let pname=this.phenotypes[i];
      let pgroup=this.phenotypesGroup[i];
//      if (r.rows[1].name != pname) {
//        console.log("I simplify: phenotype of original is not simpified: "+pname+" should be "+r.rows[1].name)
//        continue; // this phenotype is not here
//      }
      console.log("I simplify: phenotype of original matches with simpified: "+pname)
      r.phenotypes.push(pname);
      r.phenotypesIndex.push(phenotypePos); // _not_ pindex, phenotypes are simplified now // where is that simplification
      r.phenotypesGroup.push(pgroup);
      let pindex=this.phenotypesIndex[i];
      for(let n=0,
              nl=Math.min(this.rows.length,r.rows.length);
          n<nl;n++)
     {
        let tl=this.rows[n];
        for(let j=pindex,jl=pindex+4;j<jl;j++) {
          let v=tl.fields[j];
          if (typeof v == "unknown") {
             console.log("E compound("+this.id+"): copying unknown value from orig at row "+n+", pindex="+pindex);
             r.rows[n].fields.push("error in simplify");
          } else {
             r.rows[n].fields.push(v);
          }
        }
      }
      phenotypePos+=4;
    }

    // extending specification of phenotypegroup
    let al=r.rows[2].fields.length;
    console.log("I simplify ("+this.id+"): header3 ("+r.rows[2].fields+") has "+al+" columns\n");
//    if (typeof this.referenceColNo != "undefined") {
//      al=this.referenceColNo;
//    }

    let p=al-1;
    console.log("I simplify ("+this.id+"): Searching for first reasonable value for phenotypegroups in header1 ("+r.rows[0].fields+") starting as position "+p+"\n");
    while(p>10) {
      let v=r.rows[0].fields[p];
      if (typeof v != "undefined" && "" != v) {
         console.log("I simplify ("+this.id+"): breaking at position "+p+" with value '"+v+"'.");
         break;
      }
      p--;
    }
    let lastgroup=r.rows[0].fields[p];
    console.log("I simplify ("+this.id+"): Found value '"+lastgroup+"' at position "+p+"\n");

    for(let a=r.rows[0].fields.length,aval=r.rows[0].fields[a-2];a<al;a++) {
      console.log("I simplify ("+this.id+"): Extending phenotypegroup ("+aval+") specs at end of first row");
      r.rows[0].fields.push(aval);
    }

    r.header1=r.rows[0];
    r.header2=r.rows[1];
    r.header3=r.rows[2];

    return(r);
  }

  text2html(t) {
    if (typeof t == "undefined") return "";
    return(t.split("&").join("&amp;").split("<").join("&lt;"));
  }

  write(format) {
    let r = ""
    if ("text"==format || "tsv"==format) {
      if (this.pubchem) r += "\t("+this.pubchem+")";
      r += "Compound: #"+this.id+"\n";
      r += "Name:\t"+this.name+"\n";
      r += this.nlines+" entries:\n";
      for(let i=0;i<this.nlines;i++) {
        //r += "line "+i+": ";
        for(let j=0; j<this.rows[i].fields.length; j++) {
          if (j>0) {
            r += "\t";
          }
	  r += this.text2html(this.rows[i].fields[j]);
	}
        r += "\n";
      }
    }
    else if ("html"==format) {
      r += "&nbsp; <small>(";
      if (this.id > 0) {
         r += "<a href='/compound/";
         r += this.id-1;
         r += "/html'>prev</a>";
      }
      //Darn - the list of compounds is not known here
      //if (this.id < compounds.length-1) {
         if (this.id > 0) {
           r += "&nbsp;-&nbsp;";
         }
         r += "<a href='/compound/";
         r += this.id+1;
         r += "/html'>next</a>";
      //}
      r += ")</small>\n";
      r += "<h2 class=\"compoundSep\">Name: "+this.name
      if (this.pubchem) {
        r += " (<a href=\"https://https://pubchem.ncbi.nlm.nih.gov/compound/"+this.pubchem+"\">"+this.pubchem+"</a>)";
      }
      if (typeof this.url != "undefined") {
        r += " <small>(<a href=\""+this.url+"\">"+"edit</a>)</small>";
      }
      r += "</h2>\n";
      r += this.rows.length+" entries:\n";
      r += "<table class=\"singlecompound\">\n";
      r += "<thead>\n";
      // i is the row number, starting off with 0
      for(let i=0;i<this.rows.length;i++) {
        //r += "line "+i+": ";
        r += "<tr";
	if (0==i) r+= " class=\"first\"";
	else if (1==i) r+= " class=\"second\"";
	else if (2==i) r+= " class=\"third\"";
	else if (3<=i) r+= " class=\"condition\"";
	r += ">";
	switch(i) {
	  case (0):
             // the first row also does all the headers
             let pastgroup="",curgroup="";
             let span=0;
             for(let j=0,l=this.rows[i].fields.length; j<l; j++) {
	        if (j<Row.NumDescFields) {
		   let span=0;
		   switch (j) {
		     case 1:
		       r += "<th bgcolor=\"";
                       r += "yellow";
                       r += "\" colspan=6>"+this.rows[i].fields[j]+"</th>";
		       break;
		     case 2: //because of span in line 0
		     case 3:
		     case 4:
		     case 5:
		     case 6:
                       break;
		     default:
		       r += "<th>";
		       r += this.text2html(this.rows[i].fields[j])+"</th>";
		       break;
	           }
		} else {
		   curgroup=this.rows[i].fields[j];
		   if (curgroup==pastgroup) {
		     span+=1;
		   } else {
		     if ("" != pastgroup) {
		        r+= " colspan=\""+span+"\">"+pastgroup+"</th>";
		     }
		     span=1;
		     pastgroup=curgroup;
		     r += "<th class=\"phengroupName\" bgcolor=";
                     r += "\""+modDecoration.phenotypeGroupname2colour(curgroup)+"\"";
		   }
		}
             }
	     if ("" != pastgroup) r += " colspan="+span+">"+curgroup+"</th>";
	     break;
	  default:
           if (typeof this.rows[i].fields == "undefined") {
              r += "<td>this.rows["+i+"].fields is undefined</td>";
              console.log("W write("+this.id+"): this.rows["+i+"].fields is undefined");
           } else {
              //r += this.rows[i].fields.join("</td><td>");
              if (2==i) {
                 r += "<!-- "+this.rows[i].fields+"-->\n";
                 //r += "<!-- "+this.lines[i].fields+"-->\n";
              }
              for(let f=0,fl=this.rows[0].fields.length;f<fl;f++) {
                let v=this.rows[i].fields[f];
                if (typeof v == "undefined") {
                  v="";
                }
                r += "<td>"+this.text2html(v)+"</td>";
              }
           }
	}
        switch(i) {
          case (0):
            r += "<th class=\"reference\">Transcriptome</th>";
            r += "<th class=\"reference\">Proteome</th>";
            r += "<th class=\"reference\">Metabolome</th>";
            r += "<th class=\"reference\">Target</th>";
            r += "<th class=\"reference\">Reference</th>";
            r += "<th class=\"reference\">blinded</th>";
            break;
          case (1):
          case (2):
            r += "<th class=\"reference\"></th>";
            r += "<th class=\"reference\"></th>";
            r += "<th class=\"reference\"></th>";
            r += "<th class=\"reference\"></th>";
            r += "<th class=\"reference\"></th>";
            r += "<th class=\"reference\"></th>";
            break;
          default:
            r += "<td class=\"reference\">";
            if (typeof this.rows[i].transcriptome != "undefined") {
               r += this.rows[i].transcriptome;
            }
            r += "</td>";
            r += "<td class=\"reference\">";
            if (typeof this.rows[i].proteome != "undefined") {
               r += this.rows[i].proteome;
            }
            r += "</td>";
            r += "<td class=\"reference\">";
            if (typeof this.rows[i].metabolome != "undefined") {
               r += this.rows[i].metabolome;
            }
            r += "</td>";
            r += "<td class=\"reference\">";
            if (typeof this.rows[i].target != "undefined") {
               r += this.rows[i].target;
            }
            r += "</td>";
            let doi=this.rows[i].referenceDOI;
            //console.log("DOI retrieved: "+this.rows[i].referenceDOI);
            if (typeof doi == "undefined") {
               r += "<td class=\"reference\" nowrap>" + this.rows[i].reference+"</td>";
            } else {
            r += "<td class=\"reference\" nowrap>"
               + "<a href=\"https://doi.org/"+doi+"\">"+this.rows[i].reference.split("doi:")[0].split("DOI:")[0]+"</a>"
               + "</td>";
            }
            r += "<td class=\"reference\">";
            if (typeof this.rows[i].blinded != "undefined") {
               r += this.rows[i].blinded;
            }
            r += "</td>";
            break;
       
        }
        r += "</tr>\n";
        switch(i) {
          case (2):
            r += "</thead>\n";
            r += "<tbody>\n";
            break;
        }
      }
      r += "</body>\n";
      r += "</table>\n";
    }
    return(r);
  }

  // Returning an array of all strain names for which this compound was tested
  strainNamesUnique() {
    if (typeof this.strainsUnique == "undefined") {
      console.log("E ("+this.id+") modCompounds::strainNamesUnique: this.strainsUnique is undefined - creating new Array")
      this.strainsUnique = new Array();
    }
    if (0 == this.strainsUnique.length) {
      for(let i=3,l=this.rows.length;i<l;i++) {
        let s=this.rows[i].fields[strainFieldPos];
        console.log("Strain read: "+s)
        if (typeof s == "undefined") {
          console.log("E ("+this.id+") comp.strainNamesUnique: Undefined strain field for row "+i+" of "+this.name);
          continue;
        }
        let ss = s.trim();
        let p=this.strainsUnique.indexOf(ss);
        if (-1 == p) {
          this.strainsUnique.push(ss);
        }
      }
      console.log("I ("+this.id+") modCompounds::strainsUnique for compound "+this.id+": "+this.strainsUnique);
    }
    if (typeof this.strainsUnique == "undefined") {
      console.log("E ("+this.id+") modCompounds::strainsUnique for compound "+this.id+" is about to be returned as undefined %&/()");
    }
    return(this.strainsUnique);
  }

  /**
   * Creates one-line representation
   */
  exportFlat(pgl, attribute, format, printHeader) {
    if (!this.simplified) {
      console.log("E compound::exportFlat("+this.id+"): Expected simplified compound");
    }
    if (typeof attribute == "undefined") {
      console.log("E compound::exportFlat("+this.id+"): Expected attribute between 0 and 3");
    } else if (0>attribute || 3<attribute) {
      console.log("E compound::exportFlat("+this.id+"): Expected attribute between 0 and 3");
      return;
    }
    console.log("E compound::exportFlat("+this.id+","+attribute+","+printHeader+")");
    let total="";
    let lineSepEnd="\n";
    let lineSepStart="";
    let fieldSep="\t";
    let header="";
    let footer="";
    if ("html"==format) {
      header="<html><head><title>Flat - "+attribute+"</title></head><body><table>\n";
      footer="</table></body></html>";
      lineSepEnd="</td></tr>\n";
      lineSepStart="<tr><td>";
      fieldSep="</td><td>";
    }
    if (printHeader) {
      let headerPrefix = lineSepStart+"ID"+fieldSep+"PubChem CID"+fieldSep+"Compound Name";
      for(let i=0; i<Row.NumDescFields; i++) {
        //header += fieldSep + this.rows[1].fields[i]+"@"+this.rows[2].fields[i];
        let h = this.rows[2].fields[i];
        if (typeof h == "undefined") {
          console.log("E compound.exportFlat("+this.id+"): undefined header at position "+i);
        } else if (h == "") {
          console.log("E compound.exportFlat("+this.id+"): empty header at position "+i);
        } else {
          headerPrefix += fieldSep+h;
        }
      }
      let header1=headerPrefix;
      let header2=headerPrefix
      let header3=headerPrefix
      let header4=headerPrefix
      for(let pgName in pgl.groups) {
        let pg = pgl.groups[pgName];
        let phens = pg.phenotypes;
        for (let pName in phens) {
          if (typeof attribute == "undefined") {
            for(const attr of ["change","eff","signif","notes"].values()) {
              header1 += fieldSep+pgName;
              header2 += fieldSep+pName;
              header3 += fieldSep+attr;
              header4 += fieldSep+attr+"@"+pName+"@"+pgName;
            }
          } else {
             header1 += fieldSep+pgName;
             header2 += fieldSep+pName;
             header3 += fieldSep+["change","eff","signif","notes"][attribute];
             header4 += fieldSep+["change","eff","signif","notes"][attribute]+"@"+pName+"@"+pgName;
          }
        }
      }
      header1 += fieldSep+"molecular"    +fieldSep+"molecular"+fieldSep+"molecular" +fieldSep+"molecular"+fieldSep+"Reference"+fieldSep+"Reference";
      header2 += fieldSep                +fieldSep            +fieldSep             +fieldSep            +fieldSep            +fieldSep;
      header3 += fieldSep                +fieldSep            +fieldSep             +fieldSep            +fieldSep            +fieldSep;
      header4 += fieldSep+"Transcriptome"+fieldSep+"Proteome" +fieldSep+"Metabolome"+fieldSep+"Target"   +fieldSep+"Reference"+fieldSep+"blinded";

      header1 += lineSepEnd;
      header2 += lineSepEnd;
      header3 += lineSepEnd;
      header4 += lineSepEnd;
      total = header+header1+header2+header3+header4;
    }
    const prefix=lineSepStart + this.id+fieldSep+this.pubchem+fieldSep+this.name.trim();
    for(let rno=3,rl=this.rows.length;rno<rl;rno++) {
      let r=this.rows[rno];
      if (typeof(r.fields) == "undefined") {
        console.log("modCompound::exportFlat: ("+this.id+"): row "+rno+" has undefined fields array - skipping");
        continue;
      } else if (
             typeof(r.fields[1]) == "undefined"
          && typeof(r.fields[2]) == "undefined"
          && typeof(r.fields[3]) == "undefined"
          && typeof(r.fields[4]) == "undefined"
          && typeof(r.fields[5]) == "undefined"
//          && typeof(r.fields[6]) == "undefined"
      ) {
        console.log("modCompound::exportFlat: ("+this.id+"): row "+rno+" has undefined fields - skipping");
        continue;
      } else if (
             r.fields[1] == ""
          && r.fields[2] == ""
          && r.fields[3] == ""
          && r.fields[4] == ""
          && r.fields[5] == ""
 //         && r.fields[6] == ""
      ) {
        console.log("modCompound::exportFlat: ("+this.id+"): row "+rno+" has empty fields - skipping");
        continue;
      }
      let line = prefix;
      for(let i=0; i<Row.NumDescFields; i++) {
        line += fieldSep + r.fields[i];
      }
      // interpret the groups here
      for(let pgName in pgl.groups) {
        let pg = pgl.groups[pgName];
        let phens = pg.phenotypes;
        for (let pName in phens) {
          let phenPos = this.phenotypes.indexOf(pName);
          if (typeof phenPos == "undefined") {
            console.log("E compound::exportFlat("+this.id+"): phenPos is undefined for pName "+pName);
            phenPos = -1;
          }
          if (-1 == phenPos) {
            //line += fieldSep+fieldSep+fieldSep+fieldSep;
            if (typeof attribute == "undefined") {
              line += fieldSep + fieldSep + fieldSep + fieldSep + fieldSep;
            } else {
              line += fieldSep;
            }
          } else {
            let pos = this.phenotypesIndex[phenPos];
            if (typeof pos == "undefined") {
              console.log("E compound::exportFlat("+this.id+"): pos is undefined for pName "+pName);
              //line += fieldSep+fieldSep+fieldSep+fieldSep;
              line += fieldSep;
            } else {
              if (typeof attribute == "undefined") {
                //line += fieldSep+r.fields[pos+0];
                //line += fieldSep+r.fields[pos+2];
                //line += fieldSep+r.fields[pos+3];
                for (let addThis=0; addThis<4; addThis++) {
                  let v = r.fields[pos+addThis];
                  if (typeof v=="undefined") {
                    v="";
                  } else if (1 == addThis) {
                    if ("comma"==format) {
                      v=v.replace(".",",");
                    } else {
                      // v should be a .-separated decimal
                      v=v.replace(",",".");
                    }
                  }
                  line += fieldSep+v
                }
              } else {
                let v=r.fields[pos+attribute];
                if (typeof v=="undefined") {
                  v="";
                } else {
                  if (1==attribute) {
                    if ("comma"==format) {
                      v=v.replace(".",",");
                    } else {
                      // v should be a .-separated decimal
                      v=v.replace(",",".");
                    }
                  }
                }
                line += fieldSep+v;
              }
            }
          }
        }
      }
      line += fieldSep + r.transcriptome;
      line += fieldSep + r.proteome;
      line += fieldSep + r.metabolome;
      line += fieldSep + r.target;
      line += fieldSep + r.reference.trim();
      line += fieldSep + r.blinded.trim();
      line += lineSepEnd;
      total += line;
    }
    return(total);
  }

} // class

Compound.number=0;

exports.readCompoundFile = function (dir,filename) {
  let c=new Compound();
  c.readFromFileSync(dir,filename);
  Compound.number++;
  return(c);
}; 

exports.strainNameUnique = function (comp) {

  if (! comp instanceof Compound) {
    console.log("E ("+this.id+"): comp ! instanceof Compound");
    return;
  }

  return(comp.strainNameUnique());
}
